package com.morskilavovi;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * Created by filip on 06.01.18..
 */
public class Messages {

	public static void addMessage(String summary){
		FacesMessage msg = new FacesMessage(summary);
		addMessage(msg);
	}

	public static void addMessage(String summary, String detail){
		FacesMessage msg = new FacesMessage(summary, detail);
		addMessage(msg);
	}

	private static void addMessage(FacesMessage msg){
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}
}
