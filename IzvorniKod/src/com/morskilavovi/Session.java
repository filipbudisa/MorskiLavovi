package com.morskilavovi;


import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class Session {
	private static HttpSession getSession(){
		FacesContext context = FacesContext.getCurrentInstance();
		if(context == null) return null;
		return (HttpSession) context.getExternalContext().getSession(false);
	}

	public static void setAttribute(String key, Object value){
		HttpSession session = getSession();
		session.setAttribute(key, value);
	}

	public static Object getAttribute(String key){
		HttpSession session = getSession();
		if(session == null) return null;
		return session.getAttribute(key);
	}

	public static Object getAttribute(String key, HttpServletRequest req){
		HttpSession session = req.getSession(false);
		if(session == null) return null;
		return session.getAttribute(key);
	}

	public static void deleteSession(){
		Session.setAttribute("user", null);
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
	}
}
