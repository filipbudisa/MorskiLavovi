package com.morskilavovi.entities;

import java.time.LocalDate;

public class Podcilj {

    private int id;
    private int ciljId;
    private int cehId;
    private String ime;
    private boolean postignut;
    private String datum;

    public Podcilj(int id, int ciljId, int cehId, String ime, boolean postignut, String datum){
        this.id = id;
        this.ciljId = ciljId;
        this.cehId = cehId;
        this.ime = ime;
        this.postignut = postignut;
        this.datum = datum;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCiljId() {
        return ciljId;
    }

    public void setCiljId(int ciljId) {
        this.ciljId = ciljId;
    }

    public int getCehId() {
        return cehId;
    }

    public void setCehId(int cehId) {
        this.cehId = cehId;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public boolean isPostignut() {
        return postignut;
    }

    public void setPostignut(boolean postignut) {
        this.postignut = postignut;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }
}
