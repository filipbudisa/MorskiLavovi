package com.morskilavovi.entities;

import com.morskilavovi.Database;
import com.morskilavovi.enums.Svrha;

import java.time.LocalDate;
import java.util.List;

public class Cilj {

    private int id;
    private int cehId;
    private String ime;
    private boolean postignut;
    private String datum;
    private int brLikova;

    private List<Podcilj> podciljevi;
    private List<Lik> likovi;
    private Ceh ceh;

    public Cilj(int id, int cehId, String ime, boolean postignut, String datum, Svrha svrha){
        this.id = id;
        this.cehId = cehId;
        this.ime = ime;
        this.postignut = postignut;
        this.datum = datum;

        if(svrha == Svrha.CEH) return;

        if(svrha == Svrha.CILJEVI){
            this.podciljevi = Database.dohvatiPodciljeve(this.id, svrha);
            this.likovi = Database.ciljDohvatiLikove(this.id, svrha);
            this.brLikova = likovi.size();
            return;
        }

        this.ceh = Database.dohvatiCeh(this.cehId, svrha);

        if(svrha == svrha.KORISNIK) return;

        if(svrha != Svrha.CEHINFO) this.podciljevi = Database.dohvatiPodciljeve(this.id, svrha);
        this.likovi = Database.ciljDohvatiLikove(this.id, svrha);

        this.brLikova = likovi.size();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCehId() {
        return cehId;
    }

    public void setCehId(int cehId) {
        this.cehId = cehId;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public boolean isPostignut() {
        return postignut;
    }

    public void setPostignut(boolean postignut) {
        this.postignut = postignut;
    }

    public List<Podcilj> getPodciljevi() {
        return podciljevi;
    }

    public void setPodciljevi(List<Podcilj> podciljevi) {
        this.podciljevi = podciljevi;
    }

    public List<Lik> getLikovi() {
        return likovi;
    }

    public void setLikovi(List<Lik> likovi) {
        this.likovi = likovi;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public Ceh getCeh() {
        return ceh;
    }

    public void setCeh(Ceh ceh) {
        this.ceh = ceh;
    }

    public int getBrLikova() {
        return brLikova;
    }

    public void setBrLikova(int brLikova) {
        this.brLikova = brLikova;
    }
}
