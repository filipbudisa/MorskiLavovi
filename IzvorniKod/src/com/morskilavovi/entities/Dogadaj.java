package com.morskilavovi.entities;

import com.morskilavovi.Database;
import com.morskilavovi.Session;
import com.morskilavovi.enums.Svrha;

import java.util.List;

public class Dogadaj {

    private int id;
    private int userId;
    private int cehId;
    private String ime;
    private String vrijeme;
    private String opis;

    private List<Korisnik> sudionici;
    private int brojSudionika;
    private boolean korisnikPrijavljen;

    private Ceh ceh;

    public Dogadaj(int id, int cehId, String ime, String opis, String vrijeme, Svrha svrha){
        this.id = id;
        this.cehId = cehId;
        this.ime = ime;
        this.opis = opis;
        this.vrijeme = vrijeme;

        userId = (int) Session.getAttribute("user");

        korisnikPrijavljen = Database.isKorisnikPrijavljenNaDogadaj(userId, id);

        ceh = Database.dohvatiCeh(cehId, svrha);

        if(svrha == Svrha.INDEX) return;

        sudionici = Database.dogadajDohvatiSudionike(id, svrha);
        brojSudionika = sudionici.size();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCehId() {
        return cehId;
    }

    public void setCehId(int cehId) {
        this.cehId = cehId;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getVrijeme() {
        return vrijeme;
    }

    public void setVrijeme(String vrijeme) {
        this.vrijeme = vrijeme;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public Ceh getCeh() {
        return ceh;
    }

    public void setCeh(Ceh ceh) {
        this.ceh = ceh;
    }

    public List<Korisnik> getSudionici() {
        return sudionici;
    }

    public void setSudionici(List<Korisnik> sudionici) {
        this.sudionici = sudionici;
    }

    public int getBrojSudionika() {
        return brojSudionika;
    }

    public void setBrojSudionika(int brojSudionika) {
        this.brojSudionika = brojSudionika;
    }

    public boolean getKorisnikPrijavljen() {
        return korisnikPrijavljen;
    }

    public void setKorisnikPrijavljen(boolean korisnikPrijavljen) {
        this.korisnikPrijavljen = korisnikPrijavljen;
    }
}
