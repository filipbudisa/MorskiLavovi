package com.morskilavovi.entities;

public class Sudjelovanje {

    private int likId;
    private int ciljId;

    public Sudjelovanje(int likId, int ciljId){
        this.likId = likId;
        this.ciljId = ciljId;
    }

    public int getLikId() {
        return likId;
    }

    public void setLikId(int likId) {
        this.likId = likId;
    }

    public int getCiljId() {
        return ciljId;
    }

    public void setCiljId(int ciljId) {
        this.ciljId = ciljId;
    }
}
