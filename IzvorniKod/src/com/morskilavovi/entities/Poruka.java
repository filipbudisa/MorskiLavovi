package com.morskilavovi.entities;

import com.morskilavovi.Database;
import com.morskilavovi.Session;
import com.morskilavovi.enums.Svrha;

import java.time.LocalDateTime;

/**
 * Created by filip on 05.01.18..
 */
public class Poruka {

	private int id;
	private int posiljateljId;
	private int primateljId;
	private String sadrzaj;
	private String vrijeme;
	private boolean pregledano;
	private boolean mojaPoruka;

	private Korisnik sugovornik;

	public Poruka(int id, int posiljateljId, int primateljId, String sadrzaj, String vrijeme, boolean pregledano, Svrha svrha){
	    this.id = id;
		this.posiljateljId = posiljateljId;
		this.primateljId = primateljId;
		this.sadrzaj = sadrzaj;
		this.vrijeme = vrijeme;
		this.pregledano = pregledano;

		int userId = (int) Session.getAttribute("user");

        if(posiljateljId == userId){
            mojaPoruka = true;
        } else {
            mojaPoruka = false;
        }
        if(mojaPoruka) {
            sugovornik = Database.dohvatiKorisnika(primateljId, svrha);
        } else {
            sugovornik = Database.dohvatiKorisnika(posiljateljId, svrha);
        }
	}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPosiljateljId(){
		return posiljateljId;
	}

	public int getPrimateljId(){
		return primateljId;
	}

	public String getSadrzaj(){
		return sadrzaj;
	}

	public String getVrijeme(){
		return vrijeme;
	}

	public void setPosiljateljId(int posiljateljId){
		this.posiljateljId = posiljateljId;
	}

	public void setPrimateljId(int primateljId){
		this.primateljId = primateljId;
	}

	public void setSadrzaj(String sadrzaj){
		this.sadrzaj = sadrzaj;
	}

	public void setVrijeme(String vrijeme){
		this.vrijeme = vrijeme;
	}

    public boolean isPregledano() {
        return pregledano;
    }

    public void setPregledano(boolean pregledano) {
        this.pregledano = pregledano;
    }

    public boolean isMojaPoruka() {
        return mojaPoruka;
    }

    public void setMojaPoruka(boolean mojaPoruka) {
        this.mojaPoruka = mojaPoruka;
    }

    public Korisnik getSugovornik() {
        return sugovornik;
    }

    public void setSugovornik(Korisnik sugovornik) {
        this.sugovornik = sugovornik;
    }
}
