package com.morskilavovi.entities;

import com.morskilavovi.Database;
import com.morskilavovi.enums.Svrha;

import java.util.Collections;
import java.util.List;

/**
 * Created by filip on 05.01.18..
 */
public class Lik {

	private int id;
	private int userId;
	private String imeLik;
	private int igraId;
	private int klasaId;
	private int razina;
	private List<String> vjestine;

	private List<Cilj> ciljevi;
	private List<Cilj> postignuca;
	private List<Dogadaj> dogadaji;

	private Korisnik korisnik;
	private Klasa klasa;
	private Ceh ceh;
	private Igra igra;

	public Lik(int id, int userId, String imeLik, int igraId, int klasaId, int razina, List<String> vjestine, Svrha svrha){
	    this.id = id;
	    this.userId = userId;
	    this.imeLik = imeLik;
	    this.igraId = igraId;
	    this.klasaId = klasaId;
	    this.razina = razina;
	    this.vjestine = vjestine;

	    if(svrha == Svrha.CEHINFO){
			this.korisnik = Database.dohvatiKorisnika(userId, svrha);
			return;
		}

		if(svrha == Svrha.CILJEVI){
			this.korisnik = Database.dohvatiKorisnika(userId, svrha);
			return;
		}

	    if(svrha == Svrha.INDEX || svrha == Svrha.CEH || svrha == Svrha.PRIJAVA || svrha == Svrha.PRIJAVE
				|| svrha == Svrha.CEHSTVORI || svrha == Svrha.PREGLED || svrha == Svrha.CEHDOG) return;

		this.igra = Database.likDohvatiIgru(this.igraId, svrha);
		this.klasa = Database.likDohvatiKlasu(this.klasaId, svrha);

		if(svrha == Svrha.PROFIL || svrha == Svrha.CEHUREDIVANJE) return;

		this.ceh = Database.likDohvatiCeh(this.id, svrha);
		this.postignuca = Database.likDohvatiPostignuca(this.id, svrha);

		if(svrha == Svrha.KORISNIK) return;

	    this.korisnik = Database.dohvatiKorisnika(userId, svrha);
	    this.ciljevi = Database.likDohvatiCiljeve(this.id, svrha);
	    this.dogadaji = Database.likDohvatiDogadaje(this.id, svrha);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getImeLik() {
        return imeLik;
    }

    public void setImeLik(String imeLik) {
        this.imeLik = imeLik;
    }

    public int getIgraId() {
        return igraId;
    }

    public void setIgraId(int igraId) {
        this.igraId = igraId;
    }

    public int getKlasaId() {
        return klasaId;
    }

    public void setKlasaId(int klasaId) {
        this.klasaId = klasaId;
    }

    public int getRazina() {
        return razina;
    }

    public void setRazina(int razina) {
        this.razina = razina;
    }

    public List<String> getVjestine() {
        return vjestine;
    }

    public void setVjestine(List<String> vjestine) {
        this.vjestine = vjestine;
    }

    public List<Cilj> getCiljevi() {
        return ciljevi;
    }

    public void setCiljevi(List<Cilj> ciljevi) {
        this.ciljevi = ciljevi;
    }

    public List<Cilj> getPostignuca() {
	    return postignuca;
    }

    public void setPostignuca(List<Cilj> postignuca) {
        this.postignuca = postignuca;
    }

    public Klasa getKlasa() {
        return klasa;
    }

    public void setKlasa(Klasa klasa) {
        this.klasa = klasa;
    }

    public Ceh getCeh() {
        return ceh;
    }

    public void setCeh(Ceh ceh) {
        this.ceh = ceh;
    }

    public List<Dogadaj> getDogadaji() {
        return dogadaji;
    }

    public void setDogadaji(List<Dogadaj> dogadaji) {
        this.dogadaji = dogadaji;
    }

    public Igra getIgra() {
        return igra;
    }

    public void setIgra(Igra igra) {
        this.igra = igra;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }
}
