package com.morskilavovi.entities;

import com.morskilavovi.Database;
import com.morskilavovi.enums.Ovlast;
import com.morskilavovi.enums.Svrha;

public class Clanstvo {

    private int id;
    private int cehId;
    private int likId;
    private int userId;
    private Ovlast uloga;
    private Korisnik korisnik;
    private Lik lik;
    private int ovlast;

    public int getOvlast(){
        return ovlast;
    }

    public void setOvlast(int ovlast){
        this.ovlast = ovlast;
    }

    public Clanstvo(int id, int cehId, int likId, int userId, Ovlast uloga, Svrha svrha){
        this.id = id;
        this.cehId = cehId;
        this.likId = likId;
        this.userId = userId;
        this.uloga = uloga;

        this.ovlast = uloga.ordinal();

        if(svrha == Svrha.CEHINFO || svrha == Svrha.CEHDOG) return;

        this.korisnik = Database.dohvatiKorisnika(userId, svrha);
        this.lik = Database.dohvatiLika(likId, svrha);

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCehId() {
        return cehId;
    }

    public void setCehId(int cehId) {
        this.cehId = cehId;
    }

    public int getLikId() {
        return likId;
    }

    public void setLikId(int likId) {
        this.likId = likId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Ovlast getUloga() {
        return uloga;
    }

    public void setUloga(Ovlast uloga) {
        this.uloga = uloga;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public Lik getLik() {
        return lik;
    }

    public void setLik(Lik lik) {
        this.lik = lik;
    }
}
