package com.morskilavovi.entities;

import com.morskilavovi.Database;
import com.morskilavovi.enums.Svrha;

import java.util.List;

/**
 * Created by filip on 04.01.18..
 */
public class Ceh {

	private int id;
	private String naziv;
	private int igraId;
	// private Image banner;
    // private Image logo;
	private String opis;
	private List<Cilj> postignuca;
	private boolean glasanje;

	private Korisnik voda;
	private List<Korisnik> koordinatori;
	private List<Korisnik> clanovi;
	private List<Cilj> ciljevi;
	private List<Dogadaj> dogadaji;
	private List<Obavijest> obavijesti;
	private List<PrijavaUCeh> prijave;
	private List<Clanstvo> clanstva;

	private Igra igra;

	public Ceh(int id, String naziv, int igraId, String opis, List<Cilj> postignuca, boolean glasanje, Svrha svrha){
		this.id = id;
		this.naziv = naziv;
		this.igraId = igraId;
		this.opis = opis;
		this.postignuca = postignuca;
		this.glasanje = glasanje;

		if(svrha == Svrha.PREGLED){
			this.dogadaji = Database.cehDohvatiDogadaje(this.id, svrha);
			return;
		}

		if(svrha == Svrha.CEHINFO){
			this.clanstva = Database.cehDohvatiClanstva(id, svrha);
			this.ciljevi = Database.cehDohvatiCiljeve(this.id, svrha);
			return;
		}

		if(svrha == Svrha.CILJEVI){
			this.ciljevi = Database.cehDohvatiCiljeve(this.id, svrha);
			this.clanstva = Database.cehDohvatiClanstva(id, svrha);
			return;
		}


		if(svrha == Svrha.CURRENTUSER || svrha == Svrha.KORISNIK || svrha == Svrha.PROFIL
				|| svrha == Svrha.CEHUREDIVANJE || svrha == Svrha.CEHDOG) return;

        this.igra = Database.cehDohvatiIgru(this.igraId, svrha);

        if(svrha == Svrha.INDEX || svrha == Svrha.CEHOVI || svrha == Svrha.PRETRAGA || svrha == Svrha.PRIJAVA) return;

		this.clanstva = Database.cehDohvatiClanstva(id, svrha);

		if(svrha == Svrha.CEH) return;

        this.dogadaji = Database.cehDohvatiDogadaje(this.id, svrha);

        if(svrha == Svrha.PRIJAVE) {
            this.prijave = Database.dohvatiPrijave(this.id, svrha);
        }

	}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public int getIgraId() {
        return igraId;
    }

    public void setIgraId(int igraId) {
        this.igraId = igraId;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public List<Cilj> getPostignuca() {
        return postignuca;
    }

    public void setPostignuca(List<Cilj> postignuca) {
        this.postignuca = postignuca;
    }

    public boolean isGlasanje() {
        return glasanje;
    }

    public void setGlasanje(boolean glasanje) {
        this.glasanje = glasanje;
    }

    public List<Korisnik> getKoordinatori() {
        return koordinatori;
    }

    public void setKoordinatori(List<Korisnik> koordinatori) {
        this.koordinatori = koordinatori;
    }

    public List<Korisnik> getClanovi() {
        return clanovi;
    }

    public void setClanovi(List<Korisnik> clanovi) {
        this.clanovi = clanovi;
    }

    public List<Cilj> getCiljevi() {
        return ciljevi;
    }

    public void setCiljevi(List<Cilj> ciljevi) {
        this.ciljevi = ciljevi;
    }

    public List<Dogadaj> getDogadaji() {
        return dogadaji;
    }

    public void setDogadaji(List<Dogadaj> dogadaji) {
        this.dogadaji = dogadaji;
    }

    public List<Obavijest> getObavijesti() {
        return obavijesti;
    }

    public void setObavijesti(List<Obavijest> obavijesti) {
        this.obavijesti = obavijesti;
    }

    public List<PrijavaUCeh> getPrijave() {
        return prijave;
    }

    public void setPrijave(List<PrijavaUCeh> prijave) {
        this.prijave = prijave;
    }

    public Igra getIgra() {
        return igra;
    }

    public void setIgra(Igra igra) {
        this.igra = igra;
    }

    public Korisnik getVoda() {
        return voda;
    }

    public void setVoda(Korisnik voda) {
        this.voda = voda;
    }

    public List<Clanstvo> getClanstva() {
        return clanstva;
    }

    public void setClanstva(List<Clanstvo> clanstva) {
        this.clanstva = clanstva;
    }
}
