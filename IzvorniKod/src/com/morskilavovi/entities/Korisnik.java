package com.morskilavovi.entities;

import com.morskilavovi.Database;
import com.morskilavovi.enums.Svrha;

import java.util.List;

/**
 * Created by filip on 05.01.18..
 */
public class Korisnik {

	private int id;
	private String email, nadimak, about;
    // private Image avatar;
	private boolean admin;

	private List<Clanstvo> clanstva;
	private List<Lik> likovi;

	public Korisnik(int id, String email, String nadimak, String about, boolean admin, Svrha svrha){
		this.id = id;
		this.email = email;
		this.nadimak = nadimak;
		this.about = about;
		this.admin = admin;

		if(svrha == Svrha.RAZGOVORI || svrha == Svrha.RAZGOVOR || svrha == Svrha.CURRENTUSER
				|| svrha == Svrha.PRETRAGA || svrha == Svrha.GLASANJE || svrha == Svrha.PROFIL
				|| svrha == Svrha.CEH || svrha == Svrha.PREGLED || svrha == Svrha.CEHINFO
				|| svrha == Svrha.PRIJAVE || svrha == Svrha.CILJEVI) return;

		this.likovi = Database.korisnikDohvatiLikove(this.id, svrha);

		if(svrha == Svrha.KORISNIK) return;

        this.clanstva = Database.dohvatiClanstva(this.id, svrha);

	}

	public int getId(){
		return id;
	}

	public String getEmail(){
		return email;
	}

	public void setId(int id){
		this.id = id;
	}

	public void setEmail(String email){
		this.email = email;
	}

    public String getNadimak() {
        return nadimak;
    }

    public void setNadimak(String nadimak) {
        this.nadimak = nadimak;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public List<Clanstvo> getClanstva() {
        return clanstva;
    }

    public void setClanstva(List<Clanstvo> clanstva) {
        this.clanstva = clanstva;
    }

    public List<Lik> getLikovi() {
        return likovi;
    }

    public void setLikovi(List<Lik> likovi) {
        this.likovi = likovi;
    }


}
