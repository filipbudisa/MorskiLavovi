package com.morskilavovi.entities;

import com.morskilavovi.Database;
import com.morskilavovi.enums.Svrha;

import java.time.LocalDateTime;

public class Obavijest {

    private int id;
    private int cehId;
    private int autorId;
    private String sadrzaj;
    private String vrijeme;

    private Korisnik korisnik;

    public Obavijest(int id, int cehId, int autorId, String sadrzaj, String vrijeme, Svrha svrha){
        this.id = id;
        this.cehId = cehId;
        this.autorId = autorId;
        this.sadrzaj = sadrzaj;
        this.vrijeme = vrijeme;

        this.korisnik = Database.obavijestDohvatiKorisnika(this.autorId, svrha);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCehId() {
        return cehId;
    }

    public void setCehId(int cehId) {
        this.cehId = cehId;
    }

    public int getAutorId() {
        return autorId;
    }

    public void setAutorId(int autorId) {
        this.autorId = autorId;
    }

    public String getSadrzaj() {
        return sadrzaj;
    }

    public void setSadrzaj(String sadrzaj) {
        this.sadrzaj = sadrzaj;
    }

    public String getVrijeme() {
        return vrijeme;
    }

    public void setVrijeme(String vrijeme) {
        this.vrijeme = vrijeme;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }
}
