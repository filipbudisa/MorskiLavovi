package com.morskilavovi.entities;

import com.morskilavovi.Database;
import com.morskilavovi.enums.Svrha;

/**
 * Created by filip on 05.01.18..
 */
public class Klasa {

    private int id;
    private int igraId;
    private String imeKlase;

    private Igra igra;

    public Klasa(int id, int igraId, String imeKlase, Svrha svrha) {
        this.id = id;
        this.igraId = igraId;
        this.imeKlase = imeKlase;

        if(svrha == Svrha.IGRE || svrha == Svrha.KORISNIK || svrha == Svrha.PROFIL
                || svrha == Svrha.PROFIL) return;

        //this.igra = Database.klasaDohvatiIgru(this.igraId,svrha);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIgraId() {
        return igraId;
    }

    public void setIgraId(int igraId) {
        this.igraId = igraId;
    }

    public String getImeKlase() {
        return imeKlase;
    }

    public void setImeKlase(String imeKlase) {
        this.imeKlase = imeKlase;
    }

    public Igra getIgra() {
        return igra;
    }

    public void setIgra(Igra igra) {
        this.igra = igra;
    }
}
