package com.morskilavovi.entities;

public class DogadajPrijave {

    private int dogadajId;
    private int likId;

    public DogadajPrijave(int dogadajId, int likId){
        this.dogadajId = dogadajId;
        this.likId = likId;
    }

    public int getDogadajId() {
        return dogadajId;
    }

    public void setDogadajId(int dogadajId) {
        this.dogadajId = dogadajId;
    }

    public int getLikId() {
        return likId;
    }

    public void setLikId(int likId) {
        this.likId = likId;
    }
}
