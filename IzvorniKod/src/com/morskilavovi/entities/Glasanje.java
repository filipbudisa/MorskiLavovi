package com.morskilavovi.entities;

public class Glasanje {

    private int cehId;
    private int glasacId;
    private int glasId;

    public Glasanje(int cehId, int glasacId, int glasId){
        this.cehId = cehId;
        this.glasacId = glasacId;
        this.glasId = glasId;
    }

    public int getCehId() {
        return cehId;
    }

    public void setCehId(int cehId) {
        this.cehId = cehId;
    }

    public int getGlasacId() {
        return glasacId;
    }

    public void setGlasacId(int glasacId) {
        this.glasacId = glasacId;
    }

    public int getGlasId() {
        return glasId;
    }

    public void setGlasId(int glasId) {
        this.glasId = glasId;
    }
}
