package com.morskilavovi.entities;

import com.morskilavovi.Database;
import com.morskilavovi.enums.Svrha;

import java.util.List;

public class Igra {

    private int id;
    private String imeIgre;

    private List<Klasa> klase;

    public Igra(int id, String imeIgre, Svrha svrha){
        this.id = id;
        this.imeIgre = imeIgre;

        if(svrha == Svrha.INDEX || svrha == Svrha.CEHOVI || svrha == Svrha.KORISNIK
                || svrha == Svrha.PROFIL || svrha == Svrha.CEH || svrha == Svrha.PRIJAVA
                || svrha == Svrha.CEHSTVORI) return;

        this.klase = Database.dohvatiKlase(id, svrha);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImeIgre() {
        return imeIgre;
    }

    public void setImeIgre(String imeIgre) {
        this.imeIgre = imeIgre;
    }

    public List<Klasa> getKlase() {
        return klase;
    }

    public void setKlase(List<Klasa> klase) {
        this.klase = klase;
    }
}
