package com.morskilavovi.entities;

import com.morskilavovi.Database;
import com.morskilavovi.enums.Svrha;

/**
 * Created by filip on 05.01.18..
 */
public class PrijavaUCeh {

	private int likId;
	private int cehId;
	private int userId;
	private String sadrzaj;

	private Korisnik korisnik;
	private Lik lik;

	public PrijavaUCeh(int likId, int cehId, int userId, String sadrzaj, Svrha svrha){
		this.likId = likId;
		this.cehId = cehId;
		this.userId = userId;
		this.sadrzaj = sadrzaj;

		this.korisnik = Database.dohvatiKorisnika(userId,svrha);
		this.lik = Database.dohvatiLika(likId,svrha);
	}

    public int getLikId() {
        return likId;
    }

    public void setLikId(int likId) {
        this.likId = likId;
    }

    public int getCehId() {
        return cehId;
    }

    public void setCehId(int cehId) {
        this.cehId = cehId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getSadrzaj(){
		return sadrzaj;
	}

	public void setSadrzaj(String sadrzaj){
		this.sadrzaj = sadrzaj;
	}

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public Lik getLik() {
        return lik;
    }

    public void setLik(Lik lik) {
        this.lik = lik;
    }
}
