package com.morskilavovi;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;

public class SessionFilter implements Filter {
	@Override
	public void init(FilterConfig config) throws ServletException {

	}

	private static final String[] outList = { "/login.xhtml", "/register.xhtml" };
	private static final String[] ignoreList = { "/header.xhtml" };

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;

		String requested = request.getRequestURI();

		boolean isStaticResource = requested.startsWith("/javax.faces.resource/");

		if(isStaticResource || Arrays.asList(ignoreList).contains(requested)){
			chain.doFilter(req, res);
			return;
		}

		String loginUrl = request.getContextPath() + "/login.xhtml";

		boolean loggedIn = Session.getAttribute("user", request) != null;
		boolean outPage = Arrays.asList(outList).contains(requested);

		if(!loggedIn && !outPage){
			response.sendRedirect(loginUrl);
			return;
		}else if(loggedIn && outPage){
			response.sendRedirect(request.getContextPath() + "/");
			return;
		}

		chain.doFilter(req, res);
	}

	@Override
	public void destroy(){

	}
}
