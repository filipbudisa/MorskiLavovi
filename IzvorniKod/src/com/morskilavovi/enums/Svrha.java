package com.morskilavovi.enums;

public enum Svrha {
    RAZGOVORI,
    RAZGOVOR,
    IGRE,
    CEHOVI,
    CILJEVI,
    PRIJAVE,
    CEHINFO,
    PRETRAGA,
    PREGLED,
    CEHSTVORI,
    CEHUREDIVANJE,
    CURRENTUSER,
    GLASANJE,
    INDEX,
    KORISNIK,
    PROFIL,
    CEH,
    PRIJAVA,
    CEHDOG
}
