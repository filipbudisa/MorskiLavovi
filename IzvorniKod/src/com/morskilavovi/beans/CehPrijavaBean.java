package com.morskilavovi.beans;

import com.morskilavovi.Database;
import com.morskilavovi.Messages;
import com.morskilavovi.Session;
import com.morskilavovi.entities.Ceh;
import com.morskilavovi.entities.Igra;
import com.morskilavovi.entities.Korisnik;
import com.morskilavovi.entities.Lik;
import com.morskilavovi.enums.Svrha;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;

@ManagedBean(name = "cehprijavabean")
@ViewScoped
public class CehPrijavaBean {

    private int id;

    private int userId;

    private int likId;
    private String tekst;
    private Korisnik korisnik;
    private Ceh ceh;

    private List<Lik> likovi;

    public void init(){
        userId = (int) Session.getAttribute("user");
        //korisnik = Database.dohvatiKorisnika(userId, Svrha.PRIJAVE);
        ceh = Database.dohvatiCeh(id, Svrha.PRIJAVA);
        likovi = Database.igraDohvatiLikoveBezCeha(userId, ceh.getIgraId(), Svrha.PRIJAVA);
    }

    public void doCehPrijava(){

        if(!Database.dodajPrijavu(id, likId, userId, tekst)){
            Messages.addMessage("Došlo je do pogreške u spremanju podataka");
        } else {
            Messages.addMessage("Prijava poslana");
        }
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getLikId() {
        return likId;
    }

    public void setLikId(int likId) {
        this.likId = likId;
    }

    public String getTekst() {
        return "";
    }

    public void setTekst(String tekst) {
        this.tekst = tekst;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public Ceh getCeh() {
        return ceh;
    }

    public void setCeh(Ceh ceh) {
        this.ceh = ceh;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public List<Lik> getLikovi() {
        return likovi;
    }

    public void setLikovi(List<Lik> likovi) {
        this.likovi = likovi;
    }


}
