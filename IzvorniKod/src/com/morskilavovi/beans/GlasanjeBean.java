package com.morskilavovi.beans;

import com.morskilavovi.Database;
import com.morskilavovi.Messages;
import com.morskilavovi.Session;
import com.morskilavovi.entities.Korisnik;
import com.morskilavovi.enums.Svrha;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.List;

@ManagedBean(name = "glasanje")
@RequestScoped
public class GlasanjeBean {

    private int id;

    private int userId;

    private int glasId;

    private List<Korisnik> koordinatori;

    public void init(){
        userId = (int) Session.getAttribute("user");
        koordinatori = Database.cehDohvatiKoordinatore(id, Svrha.GLASANJE);
    }

    public void doGlasaj(){
        if(!Database.dajGlas(id, userId, glasId)){
            Messages.addMessage("Došlo je do greške kod spremanja podataka");
        }

        Database.provjeriGlasove(id);
    }

    public int getGlasId() {
        return glasId;
    }

    public void setGlasId(int glasId) {
        this.glasId = glasId;
    }

    public List<Korisnik> getKoordinatori() {
        return koordinatori;
    }

    public void setKoordinatori(List<Korisnik> koordinatori) {
        this.koordinatori = koordinatori;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
