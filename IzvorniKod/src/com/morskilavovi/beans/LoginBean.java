package com.morskilavovi.beans;


import com.morskilavovi.Database;
import com.morskilavovi.Messages;
import com.morskilavovi.Session;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "login")
@ViewScoped
public class LoginBean {

	private String username;
	private String password;

    public String doLogin(){
        int id = Database.tryLogin(username, password);

        if(id == -1){
            Messages.addMessage("Netočno korisničko ime ili lozinka");
            return "login";
        }else{
            Session.setAttribute("user", id);
            return "index?faces-redirect=true";
        }
    }

	public String getUsername(){
		return "";
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getPassword(){
		return "";
	}

	public void setPassword(String password){
		this.password = password;
	}

}
