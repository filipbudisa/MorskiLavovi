package com.morskilavovi.beans;

import com.morskilavovi.Database;
import com.morskilavovi.Messages;
import com.morskilavovi.Session;
import com.morskilavovi.entities.Ceh;
import com.morskilavovi.entities.Clanstvo;
import com.morskilavovi.entities.Lik;
import com.morskilavovi.entities.Obavijest;
import com.morskilavovi.enums.Svrha;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.util.List;

@ManagedBean(name = "cehpregled")
@ViewScoped
public class CehPregledBean {

    private int id;

    private int userId;
    private int obavijestCehId;
    private int dogadajPrijavaId;
    private int dogadajOdjavaId;

    private String dogadajNaslov;
    private String dogadajDatum;
    private String obavijestTekst;

    private Lik lik;
    private Ceh ceh;
    private List<Obavijest> obavijesti;

    public void init(){
        userId = (int) Session.getAttribute("user");
        ceh = Database.dohvatiCeh(id, Svrha.PREGLED);
        obavijesti = Database.cehDohvatiObavijesti(id, Svrha.PREGLED);

        Clanstvo clanstvo = Database.dohvatiClanstvo(userId, id, Svrha.CEHINFO);
        if(clanstvo != null) lik = Database.dohvatiLika(clanstvo.getLikId(), Svrha.PREGLED);
    }

    public String dodajObavijest(){
        userId = (int) Session.getAttribute("user");
        if(!Database.dodajObavijest(id, userId, obavijestTekst)){
            Messages.addMessage("Greška kod dodavanja obavijesti");
        } else {
            Messages.addMessage("Obavijest uspješno dodana");
        }

        return "ceh?id=" + id;
    }

    public String doDogadajPrijava(){
    	String dogId = FacesContext.getCurrentInstance().
				getExternalContext().getRequestParameterMap().get("dogadajPrijavaId");

        if(!Database.dodajDogadajPrijava(lik.getId(), Integer.parseInt(dogId))){
            Messages.addMessage("Greška kod prijavljivanja na događaj");
        } else {
            Messages.addMessage("Uspješno ste prijavljeni na događaj");
        }
        return "ceh?faces-redirect=true&id=" + id;
    }

    public String doDogadajOdjava(){
		String dogId = FacesContext.getCurrentInstance().
				getExternalContext().getRequestParameterMap().get("dogadajOdjavaId");

        if(!Database.obrisiDogadajPrijava(lik.getId(), Integer.parseInt(dogId))){
            Messages.addMessage("Greška kod odjavljivanja sa događaja");
        } else {
            Messages.addMessage("Uspješno ste odjavljeni sa događaja");
        }
        return "ceh?faces-redirect=true&id=" + id;
    }

    public void dodajDogadaj(){

        if(!Database.dodajDogadaj(obavijestCehId, dogadajNaslov, dogadajDatum)){
            Messages.addMessage("Greška kod dodavanja događaja");
        } else {
            Messages.addMessage("Uspješno ste dodali događaj");
        }

    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setCehId(int obavijestCehId) {
        this.obavijestCehId = obavijestCehId;
    }

	public int getDogadajPrijavaId(){
		return dogadajPrijavaId;
	}

	public void setDogadajPrijavaId(int dogadajPrijavaId){
		this.dogadajPrijavaId = dogadajPrijavaId;
	}

	public int getDogadajOdjavaId(){
		return dogadajOdjavaId;
	}

	public void setDogadajOdjavaId(int dogadajOdjavaId){
		this.dogadajOdjavaId = dogadajOdjavaId;
	}

	public Ceh getCeh() {
        return ceh;
    }

    public void setCeh(Ceh ceh) {
        this.ceh = ceh;
    }

    public List<Obavijest> getObavijesti() {
        return obavijesti;
    }

    public void setObavijesti(List<Obavijest> obavijesti) {
        this.obavijesti = obavijesti;
    }

    public String getDogadajNaslov() {
        return dogadajNaslov;
    }

    public void setDogadajNaslov(String dogadajNaslov) {
        this.dogadajNaslov = dogadajNaslov;
    }

    public String getDogadajDatum() {
        return dogadajDatum;
    }

    public void setDogadajDatum(String dogadajDatum) {
        this.dogadajDatum = dogadajDatum;
    }

    public String getObavijestTekst() {
        return obavijestTekst;
    }

    public void setObavijestTekst(String obavijestTekst) {
        this.obavijestTekst = obavijestTekst;
    }

    public Lik getLik() {
        return lik;
    }

    public void setLik(Lik lik) {
        this.lik = lik;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public int getObavijestCehId() {
        return obavijestCehId;
    }

    public void setObavijestCehId(int obavijestCehId) {
        this.obavijestCehId = obavijestCehId;
    }
}
