package com.morskilavovi.beans;

import com.morskilavovi.Database;
import com.morskilavovi.entities.Dogadaj;
import com.morskilavovi.entities.Korisnik;
import com.morskilavovi.entities.Ceh;
import com.morskilavovi.entities.Lik;
import com.morskilavovi.enums.Svrha;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.Collections;
import java.util.List;

/**
 * Created by filip on 04.01.18..
 */
@ManagedBean(name = "index")
@ViewScoped
public class IndexBean {

    private int id;
    private List<Lik> likovi;
    private List<Dogadaj> dogadaji;

    public void init(){
        likovi = Database.korisnikDohvatiLikove(id, Svrha.INDEX);

        for(Lik lik : likovi){

            List<Dogadaj> likDogadaji = Database.likDohvatiNadolazeceDogadaje(lik.getId(), Svrha.INDEX);
            dogadaji.addAll(likDogadaji);
        }

    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Lik> getLikovi() {
        return likovi;
    }

    public void setLikovi(List<Lik> likovi) {
        this.likovi = likovi;
    }
    
    public int getId() {
        return id;
    }

    public List<Dogadaj> getDogadaji() {
        return dogadaji;
    }

    public void setDogadaji(List<Dogadaj> dogadaji) {
        this.dogadaji = dogadaji;
    }
}
