package com.morskilavovi.beans;

import com.morskilavovi.Database;
import com.morskilavovi.entities.Ceh;
import com.morskilavovi.enums.Svrha;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;

@ManagedBean(name = "cehovi")
@ViewScoped
public class CehoviBean {

    private int id;

    private List<Ceh> cehovi;

    public void init(){
        cehovi = Database.korisnikDohvatiCehove(id, Svrha.CEHOVI);
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Ceh> getCehovi() {
        return cehovi;
    }

    public void setCehovi(List<Ceh> cehovi) {
        this.cehovi = cehovi;
    }
}
