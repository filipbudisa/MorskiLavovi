package com.morskilavovi.beans;

import com.morskilavovi.Database;
import com.morskilavovi.Messages;
import com.morskilavovi.Session;
import com.morskilavovi.entities.Korisnik;
import com.morskilavovi.entities.Poruka;
import com.morskilavovi.enums.Svrha;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.List;

@ManagedBean(name = "razgovor")
@RequestScoped
public class RazgovorBean {

    private int korisnikId;
    private int korisnik2Id;

    private Korisnik korisnik;
    private Korisnik korisnik2;
    private List<Poruka> poruke;
    private String novaPoruka;

    private int offset;
    private int increase = 10;

    public void init(){
        korisnikId = (int) Session.getAttribute("user");
        korisnik = Database.dohvatiKorisnika(korisnikId, Svrha.RAZGOVOR);
        korisnik2 = Database .dohvatiKorisnika(korisnik2Id, Svrha.RAZGOVOR);
        poruke = Database.dohvatiRazgovor(korisnikId, korisnik2Id, increase, offset, Svrha.RAZGOVOR);
    }

    public String posaljiPoruku(){
        if(!Database.dodajPoruku(korisnikId, korisnik2Id, novaPoruka)){
            Messages.addMessage("Došlo je do greške kod slanja poruke");
        }

        return "razgovor?faces-redirect=true&includeViewParams=true";
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public void setKorisnikId(int korisnikId) {
        this.korisnikId = korisnikId;
    }

    public void setKorisnik2Id(int korisnik2Id) {
        this.korisnik2Id = korisnik2Id;
    }

    public List<Poruka> getPoruke() {
        return poruke;
    }

    public void setPoruke(List<Poruka> poruke) {
        this.poruke = poruke;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public Korisnik getKorisnik2() {
        return korisnik2;
    }

    public void setKorisnik2(Korisnik korisnik2) {
        this.korisnik2 = korisnik2;
    }

    public int getKorisnikId() {
        return korisnikId;
    }

    public int getKorisnik2Id() {
        return korisnik2Id;
    }

    public String getNovaPoruka() {
        return "";
    }

    public void setNovaPoruka(String novaPoruka) {
        this.novaPoruka = novaPoruka;
    }

    public int getOffset() {
        return offset;
    }

    public int getIncrease() {
        return increase;
    }

    public void setIncrease(int increase) {
        this.increase = increase;
    }
}
