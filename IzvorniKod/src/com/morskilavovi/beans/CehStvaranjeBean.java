package com.morskilavovi.beans;

import com.morskilavovi.Database;
import com.morskilavovi.Messages;
import com.morskilavovi.Session;
import com.morskilavovi.entities.Igra;
import com.morskilavovi.entities.Korisnik;
import com.morskilavovi.entities.Lik;
import com.morskilavovi.enums.Ovlast;
import com.morskilavovi.enums.Svrha;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ManagedBean(name = "cehstvaranjeb")
@RequestScoped
public class CehStvaranjeBean {

    private int userId;

    private String naziv;
    private int igraId;
    private int likId;

    private Korisnik korisnik;
    private List<Igra> igreLikBezCeha;
    private List<Lik> likoviIgre;

    public void init(){
        userId = (int) Session.getAttribute("user");
        //korisnik = Database.dohvatiKorisnika(userId, Svrha.CEHSTVORI);

        igreLikBezCeha = Database.dohvatiIgreZaLikoveBezCeha(userId, Svrha.CEHSTVORI);

        likoviIgre = new ArrayList<>();

        for(Igra igra : igreLikBezCeha){
            likoviIgre.addAll(Database.igraDohvatiLikoveBezCeha(userId, igra.getId(), Svrha.CEHSTVORI));
        }
    }

    public void doStvoriCeh(){
        boolean cehId = Database.stvoriCeh(naziv, igraId, likId);

        if(!cehId){
            Messages.addMessage("Došlo je do pogreške u spremanju podataka");
            return;
        }

        Messages.addMessage("Ceh je stvoren");
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getNaziv() {
        return "";
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public int getIgraId() {
        return igraId;
    }

    public void setIgraId(int igraId) {
        this.igraId = igraId;
    }

    public int getLikId() {
        return likId;
    }

    public void setLikId(int likId) {
        this.likId = likId;
    }

    public int getUserId() {
        return userId;
    }

    public List<Igra> getIgreLikBezCeha() {
        return igreLikBezCeha;
    }

    public void setIgreLikBezCeha(List<Igra> igreLikBezCeha) {
        this.igreLikBezCeha = igreLikBezCeha;
    }

    public List<Lik> getLikoviIgre() {
        return likoviIgre;
    }

    public void setLikoviIgre(List<Lik> likoviIgre) {
        this.likoviIgre = likoviIgre;
    }
}
