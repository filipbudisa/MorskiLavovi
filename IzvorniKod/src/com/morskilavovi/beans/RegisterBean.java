package com.morskilavovi.beans;

import com.morskilavovi.Database;
import com.morskilavovi.Messages;
import com.morskilavovi.Session;
import com.morskilavovi.entities.Korisnik;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.Objects;

@ManagedBean(name = "register")
@SessionScoped
public class RegisterBean {

    private String username;
    private String email;
    private String password;
    private String password2;

    public String doRegister(){
        if (username.equals("") || email.equals("") || password.equals("") || password2.equals("")){
            Messages.addMessage("Svi podatci moraju biti upisani");
            return "register";
        }

        if(Objects.equals(password, password2)) {
            int id = Database.tryRegister(username, email, password);

            if(id == -1 || id == -2 || id == -3){
                Messages.addMessage("Korisničko ime ili email već postoji");
                return "register";
            } else {
                Session.setAttribute("user", id);
                return "index?faces-redirect=true";
            }

        } else {
            Messages.addMessage("Lozinke ne odgovaraju");
            return "register";
        }
    }

    public String getUsername() {
        return "";
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return "";
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return "";
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword2() {
        return "";
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

}
