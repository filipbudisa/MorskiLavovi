package com.morskilavovi.beans;

import com.morskilavovi.Database;
import com.morskilavovi.Messages;
import com.morskilavovi.Session;
import com.morskilavovi.entities.Ceh;
import com.morskilavovi.entities.Clanstvo;
import com.morskilavovi.enums.Ovlast;
import com.morskilavovi.enums.Svrha;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "cehinfo")
@ViewScoped
public class CehInfoBean {

    private int id;

    private int userId;

    private Ovlast ovlastTrenutnogOv;
    private int ovlastTrenutnog;

    private int clanOvlastId;

    private Ceh ceh;
    private Clanstvo clanstvo;

    public int getOvlastTrenutnog(){
        return ovlastTrenutnog;
    }

    public void setOvlastTrenutnog(int ovlastTrenutnog){
        this.ovlastTrenutnog = ovlastTrenutnog;
    }

    public void init(){
        userId = (int) Session.getAttribute("user");
        clanstvo = Database.dohvatiClanstvo(userId, id, Svrha.CEHINFO);

        if(clanstvo == null) return;

        ovlastTrenutnogOv = clanstvo.getUloga();
        ovlastTrenutnog = ovlastTrenutnogOv.ordinal();


        ceh = Database.dohvatiCeh(id, Svrha.CEHINFO);
    }

    public void doClanUp(){

        if(!Database.clanUpgrade(clanOvlastId)){
            Messages.addMessage("Greška kod spremanja podataka");
        } else {
            Messages.addMessage("Uspješno spremljeni podatci");
        }
    }

    public void doClanDown(){

        if(!Database.clanDowngrade(clanOvlastId)){
            Messages.addMessage("Greška kod spremanja podataka");
        } else {
            Messages.addMessage("Uspješno spremljeni podatci");
        }
    }

    public Ceh getCeh() {
        return ceh;
    }

    public void setCeh(Ceh ceh) {
        this.ceh = ceh;
    }

    public int getClanOvlastId() {
        return clanOvlastId;
    }

    public void setClanOvlastId(int clanOvlastId) {
        this.clanOvlastId = clanOvlastId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Ovlast getOvlastTrenutnogOv() {
        return ovlastTrenutnogOv;
    }

    public void setOvlastTrenutnogOv(Ovlast ovlastTrenutnogOv) {
        this.ovlastTrenutnogOv = ovlastTrenutnogOv;
    }

    public Clanstvo getClanstvo() {
        return clanstvo;
    }

    public void setClanstvo(Clanstvo clanstvo) {
        this.clanstvo = clanstvo;
    }
}
