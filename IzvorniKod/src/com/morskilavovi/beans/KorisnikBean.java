package com.morskilavovi.beans;

import com.morskilavovi.Database;
import com.morskilavovi.entities.Korisnik;
import com.morskilavovi.enums.Svrha;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean(name = "korisnik")
@RequestScoped
public class KorisnikBean {

    private int id;

    private Korisnik korisnik;

    public void init(){
        korisnik = Database.dohvatiKorisnika(id, Svrha.KORISNIK);
    }

    public Korisnik getKorisnik(){
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik){
        this.korisnik = korisnik;
    }

    public void setId(int id){
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
