package com.morskilavovi.beans;

import com.morskilavovi.Database;
import com.morskilavovi.Session;
import com.morskilavovi.entities.Ceh;
import com.morskilavovi.entities.Cilj;
import com.morskilavovi.enums.Svrha;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;

@ManagedBean(name = "ceh")
@ViewScoped
public class CehBean {

    private int id;
    private int userId;
    private boolean isKorisnikClan;

    private Ceh ceh;

    private List<Cilj> postignuca;

    public void init(){
        userId = (int) Session.getAttribute("user");
        isKorisnikClan = Database.isKorisnikClan(userId, id);
        ceh = Database.dohvatiCeh(id, Svrha.CEH);

        List<String> listaPostignuca = Database.dohvatiIstaknutaPostignucaZaCeh(id);

        for(Cilj cilj : ceh.getPostignuca()){
            if(listaPostignuca.contains(cilj.getId())){
                postignuca.add(cilj);
            }
        }
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Ceh getCeh() {
        return ceh;
    }

    public void setCeh(Ceh ceh) {
        this.ceh = ceh;
    }

    public boolean isKorisnikClan() {
        return isKorisnikClan;
    }

    public void setKorisnikClan(boolean korisnikClan) {
        isKorisnikClan = korisnikClan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
