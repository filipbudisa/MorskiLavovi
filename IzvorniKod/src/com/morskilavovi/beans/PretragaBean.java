package com.morskilavovi.beans;

import com.morskilavovi.Database;
import com.morskilavovi.PretragaItem;
import com.morskilavovi.entities.Ceh;
import com.morskilavovi.entities.Korisnik;
import com.morskilavovi.enums.ItemPretraga;
import com.morskilavovi.enums.Svrha;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.ArrayList;
import java.util.List;

@ManagedBean(name = "pretragab")
@RequestScoped
public class PretragaBean {

    private String tekst;
    private List<PretragaItem> items;

    public void init(){
        items = new ArrayList<>();

        searchKorisnici();
        searchCehovi();
    }

    public void searchKorisnici(){

        ArrayList<Korisnik> korisnici = Database.pronadiKorisnike(tekst, Svrha.PRETRAGA);

        for(Korisnik korisnik : korisnici) {
            items.add(new PretragaItem(ItemPretraga.KORISNIK, korisnik, null));
        }
    }

    public void searchCehovi(){

        ArrayList<Ceh> cehovi = Database.pronadiCehove(tekst, Svrha.PRETRAGA);

        for(Ceh ceh : cehovi) {
            items.add(new PretragaItem(ItemPretraga.CEH, null, ceh));
        }
    }

    public String getTekst() {
        return tekst;
    }

    public void setTekst(String tekst) {
        this.tekst = tekst;
    }

    public List<PretragaItem> getItems() {
        return items;
    }

    public void setItems(List<PretragaItem> items) {
        this.items = items;
    }
}
