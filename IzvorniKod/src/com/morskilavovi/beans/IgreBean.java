package com.morskilavovi.beans;

import com.morskilavovi.Database;
import com.morskilavovi.Messages;
import com.morskilavovi.entities.Igra;
import com.morskilavovi.enums.Svrha;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ManagedBean(name = "igre")
@ViewScoped
public class IgreBean {

    private int id;

    private List<Igra> igre;

    private String obrisaneIgre;
    private String obrisaneKlase;
    private String izmjeneIgra;

    public void init(){
        igre = Database.dohvatiIgre(Svrha.IGRE);
    }

    public int getId(){
        return id;
    }

    public void doSpremiIgre(){
        if(!obrisaneIgre.equals("")){
            List<String> igraId = Arrays.asList(obrisaneIgre.trim().split(","));

            for(String id : igraId){
                boolean isDeleted = Database.obrisiIgru(Integer.parseInt(id));

                if(!isDeleted){
                    Messages.addMessage("Došlo je do pogreške u spremanju podataka");
                }
            }
        }

        if(!obrisaneKlase.equals("")){
            List<String> klasaId = Arrays.asList(obrisaneKlase.trim().split(","));

            for(String id : klasaId){
                boolean isDeleted = Database.obrisiKlasu(Integer.parseInt(id));

                if(!isDeleted){
                    Messages.addMessage("Došlo je do pogreške u spremanju podataka");
                }
            }
        }

        if(!izmjeneIgra.isEmpty()){

            try {
                JSONArray arr = new JSONArray(izmjeneIgra);

                for(int j = 0; j < arr.length(); j++) {

                    JSONObject obj = new JSONObject(arr.getJSONObject(j));

                    if (obj.getBoolean("novaIgra")) {
                        String naziv = obj.getString("naziv");
                        List<String> noveKlase = new ArrayList<>();
                        JSONArray array = obj.getJSONArray("noveKlase");
                        for (int i = 0; i < array.length(); i++) {
                            noveKlase.add(array.getString(i));
                        }

                        if (!Database.dodajIgru(naziv)) {
                            Messages.addMessage("Došlo je do pogreške u spremanju podataka");
                        }

                        int idIgre = Database.dohvatiIdIgre(naziv);

                        if (!Database.dodajKlase(idIgre, noveKlase)) {
                            Messages.addMessage("Došlo je do pogreške u spremanju podataka");
                        }
                    } else {
                        int idIgre = obj.getInt("id");
                        List<String> noveKlase = new ArrayList<>();
                        JSONArray array = obj.getJSONArray("noveKlase");
                        for (int i = 0; i < array.length(); i++) {
                            noveKlase.add(array.getString(i));
                        }

                        if (!Database.dodajKlase(idIgre, noveKlase)) {
                            Messages.addMessage("Došlo je do pogreške u spremanju podataka");
                        }
                    }
                }

            } catch(Exception e){
                Messages.addMessage("Došlo je do pogreške u spremanju podataka");
            }

        }

        Messages.addMessage("Podatci su uspješno spremljeni");
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Igra> getIgre() {
        return igre;
    }

    public void setIgre(List<Igra> igre) {
        this.igre = igre;
    }

    public String getObrisaneIgre() {
        return "";
    }

    public void setObrisaneIgre(String obrisaneIgre) {
        this.obrisaneIgre = obrisaneIgre;
    }

    public String getObrisaneKlase() {
        return "";
    }

    public void setObrisaneKlase(String obrisaneKlase) {
        this.obrisaneKlase = obrisaneKlase;
    }

    public String getIzmjeneIgra() {
        return izmjeneIgra;
    }

    public void setIzmjeneIgra(String izmjeneIgra) {
        this.izmjeneIgra = izmjeneIgra;
    }
}
