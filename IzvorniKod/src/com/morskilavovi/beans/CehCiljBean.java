package com.morskilavovi.beans;

import com.morskilavovi.Database;
import com.morskilavovi.Messages;
import com.morskilavovi.entities.Ceh;
import com.morskilavovi.enums.Svrha;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

@ManagedBean(name = "cehciljevi")
@ViewScoped
public class CehCiljBean {

    private int id;

    private int ispunjenCiljId;

    private String obrisanaSudjelovanja;
    private String obrisaniPodciljevi;
    private String izmjeneCiljeva;
    private String novi_cilj_naziv;

    private Ceh ceh;

    public void init(){
        ceh = Database.dohvatiCeh(id, Svrha.CILJEVI);
    }

    public void doSpremiCiljeve(){

        if(!obrisaniPodciljevi.equals("")){

            List<String> obrisaniId = Arrays.asList(obrisaniPodciljevi.trim().split(","));

            for(String id : obrisaniId){

                if(!Database.obrisiPodcilj(Integer.parseInt(id))){
                    Messages.addMessage("Došlo je do pogreške kod spremanja ciljeva");
                }
            }

        }

        if (!obrisanaSudjelovanja.equals("")) {
            try {
                JSONObject obj = new JSONObject(obrisanaSudjelovanja);

                Iterator<?> keys = obj.keys();

                while( keys.hasNext() ) {
                    String key = (String)keys.next();
                    if ( obj.get(key) instanceof JSONObject ) {
                        JSONArray arr = new JSONArray(obj.get(key));
                        for(int i = 0; i < arr.length(); i++){
                            if(!Database.obrisiSudjelovanje(Integer.parseInt(key), arr.getInt(i))){
                                Messages.addMessage("Došlo je do pogreške kod spremanja cilja");
                            }
                        }
                    }
                }

            } catch (Exception e) {
                Messages.addMessage("Došlo je do pogreške kod spremanja cilja");
            }
        }

        if(!izmjeneCiljeva.equals("")){

            try {
                JSONArray arr = new JSONArray(izmjeneCiljeva);

                for(int i = 0; i < arr.length(); i++){
                    JSONObject object = new JSONObject(arr.getJSONObject(i));

                    int id = object.getInt("id");

                    JSONArray sudionici = object.getJSONArray("sudionici");

                    for(int j = 0; j < sudionici.length(); j++){
                        List<Integer> trenutni = Database.dohvatiSudionikeId(id);
                        if (!trenutni.contains(sudionici.getInt(j))){
                            if(!Database.dodajSudionika(sudionici.getInt(j), id)){
                                Messages.addMessage("Došlo je do pogreške kod spremanja cilja");
                            }
                        }
                    }

                    JSONArray noviPodciljevi = object.getJSONArray("noviPodciljevi");

                    for(int j = 0; j < noviPodciljevi.length(); j++){

                        if(!Database.dodajPodcilj(noviPodciljevi.getString(j), id)){
                            Messages.addMessage("Došlo je do pogreške kod spremanja cilja");
                        }
                    }


                    JSONArray podciljevi = object.getJSONArray("ispunjen");

                    for(int j = 0; j < podciljevi.length(); j++){
                        JSONObject obj = podciljevi.getJSONObject(j);

                        int podciljId = obj.getInt("id");
                        boolean ispunjen = obj.getBoolean("ispunjen");

                        if(ispunjen){
                            if(!Database.ispuniPodcilj(podciljId)){
                                Messages.addMessage("Došlo je do pogreške kod spremanja cilja");
                            }
                        }
                    }

                }

            } catch (Exception e){
                Messages.addMessage("Došlo je do pogreške kod spremanja cilja");
            }

        }

        Messages.addMessage("Cilj je uspješno spremljen");


    }

    public void dodajCilj(){

        if(!Database.dodajCilj(id, novi_cilj_naziv)){
            Messages.addMessage("Došlo je do problema kod dodavanja cilja");
        } else {
            Messages.addMessage("Cilj je uspješno dodan");
        }
    }

    public void doCiljIspunjen(){

        if(!Database.ispuniCilj(ispunjenCiljId)){
            Messages.addMessage("Došlo je do pogreške");
        } else {
            Messages.addMessage("Cilj ispunjen");
        }
    }

    public int getIspunjenCiljId() {
        return ispunjenCiljId;
    }

    public void setIspunjenCiljId(int ispunjenCiljId) {
        this.ispunjenCiljId = ispunjenCiljId;
    }

    public String getObrisanaSudjelovanja() {
        return obrisanaSudjelovanja;
    }

    public void setObrisanaSudjelovanja(String obrisanaSudjelovanja) {
        this.obrisanaSudjelovanja = obrisanaSudjelovanja;
    }

    public String getObrisaniPodciljevi() {
        return obrisaniPodciljevi;
    }

    public void setObrisaniPodciljevi(String obrisaniPodciljevi) {
        this.obrisaniPodciljevi = obrisaniPodciljevi;
    }

    public String getIzmjeneCiljeva() {
        return izmjeneCiljeva;
    }

    public void setIzmjeneCiljeva(String izmjeneCiljeva) {
        this.izmjeneCiljeva = izmjeneCiljeva;
    }

    public String getNovi_cilj_naziv() {
        return novi_cilj_naziv;
    }

    public void setNovi_cilj_naziv(String novi_cilj_naziv) {
        this.novi_cilj_naziv = novi_cilj_naziv;
    }

    public Ceh getCeh() {
        return ceh;
    }

    public void setCeh(Ceh ceh) {
        this.ceh = ceh;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
