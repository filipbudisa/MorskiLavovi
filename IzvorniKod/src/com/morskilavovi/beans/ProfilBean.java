package com.morskilavovi.beans;

import com.morskilavovi.Database;
import com.morskilavovi.Messages;
import com.morskilavovi.Session;
import com.morskilavovi.entities.Igra;
import com.morskilavovi.entities.Klasa;
import com.morskilavovi.entities.Korisnik;
import com.morskilavovi.entities.Lik;
import com.morskilavovi.enums.Svrha;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ManagedBean(name = "profil")
@ViewScoped
public class ProfilBean {

    private int id;
    private String obrisaniLikovi;
    private String likovi;
    private String opis;

    private String likoviKlase;

    private Korisnik korisnik;
    private List<Lik> userLikovi;
    private List<Igra> igre;

    public void init(){
        id = (int) Session.getAttribute("user");

        userLikovi = Database.korisnikDohvatiLikove(id, Svrha.PROFIL);
        korisnik = Database.dohvatiKorisnika(id, Svrha.PROFIL);
        igre = Database.dohvatiIgre(Svrha.PROFIL);

        opis = korisnik.getAbout();


        JSONObject obj = new JSONObject();

        JSONObject object = new JSONObject();

        try {
            for (Igra igra : igre) {

                List<Klasa> igreKlase = Database.dohvatiKlase(igra.getId(), Svrha.PROFIL);

                object.put("id", igra.getId());

                object.put("naziv", igra.getImeIgre());

                JSONArray klase = new JSONArray();

                JSONObject klasa = new JSONObject();

                for(Klasa igreKlasa : igreKlase) {
                    klasa.put("id", igreKlasa.getId());
                    klasa.put("ime", igreKlasa.getImeKlase());
                }

                klase.put(klasa);

                object.put("klase", klase);

                obj.put("", object);
            }

        } catch (Exception e){

        }

        likoviKlase = obj.toString();
    }

    public void doPohrani(){
        if (!opis.equals("")){
            if (!Database.korisnikAzurirajOpis(opis, id)){
                Messages.addMessage("Došlo je do pogreške u spremanju podataka");
            }
        }

        if (!obrisaniLikovi.equals("")){
            List<String> obrisaniId = Arrays.asList(obrisaniLikovi.trim().split(","));

            for(String id : obrisaniId){
                boolean isDeleted = Database.obrisiLik(Integer.parseInt(id));

                if(!isDeleted){
                    Messages.addMessage("Došlo je do pogreške u spremanju podataka");
                }
            }
        }

        if (!likovi.equals("")){

            try {
                JSONArray array = new JSONArray(likovi);

                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = new JSONObject(array.get(i));

                    if (obj.getBoolean("noviLik")) {
                        List<String> vjestine = new ArrayList<>();
                        int razina = obj.getInt("razina");

                        JSONArray arr = obj.getJSONArray("vjestine");
                        for (int j = 0; j < arr.length(); j++) {
                            vjestine.add(arr.getString(j));
                        }

                        String naziv = obj.getString("naziv");
                        int igra = obj.getInt("igra");
                        int klasa = obj.getInt("klasa");

                        if (!Database.dodajLika(id, razina, vjestine, naziv, igra, klasa)) {
                            Messages.addMessage("Došlo je do pogreške u spremanju podataka");
                        }

                    } else {
                        List<String> vjestine = new ArrayList<>();
                        int id = obj.getInt("id");
                        int razina = obj.getInt("razina");

                        JSONArray arr = obj.getJSONArray("vjestine");
                        for (int j = 0; j < arr.length(); j++) {
                            vjestine.add(arr.getString(j));
                        }

                        if (!Database.azurirajLika(this.id, id, razina, vjestine)) {
                            Messages.addMessage("Došlo je do pogreške u spremanju podataka");
                        }
                    }
                }
            } catch(Exception e){
                Messages.addMessage("Došlo je do pogreške u spremanju podataka");
            }
        }

        Messages.addMessage("Lik je uspješno spremljen");
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getObrisaniLikovi() {
        return "";
    }

    public void setObrisaniLikovi(String obrisaniLikovi) {
        this.obrisaniLikovi = obrisaniLikovi;
    }

    public String getLikovi() {
        return "";
    }

    public void setLikovi(String likovi) {
        this.likovi = likovi;
    }

    public List<Lik> getUserLikovi() {
        return userLikovi;
    }

    public void setUserLikovi(List<Lik> userLikovi) {
        this.userLikovi = userLikovi;
    }

    public int getId() {
        return id;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public String getLikoviKlase() {
        return likoviKlase;
    }

    public void setLikoviKlase(String likoviKlase) {
        this.likoviKlase = likoviKlase;
    }

    public List<Igra> getIgre() {
        return igre;
    }

    public void setIgre(List<Igra> igre) {
        this.igre = igre;
    }
}
