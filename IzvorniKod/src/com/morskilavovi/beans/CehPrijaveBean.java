package com.morskilavovi.beans;

import com.morskilavovi.Database;
import com.morskilavovi.Messages;
import com.morskilavovi.entities.Korisnik;
import com.morskilavovi.entities.Lik;
import com.morskilavovi.entities.PrijavaUCeh;
import com.morskilavovi.enums.Ovlast;
import com.morskilavovi.enums.Svrha;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.List;

@ManagedBean(name = "cehprijaveb")
@RequestScoped
public class CehPrijaveBean {

    private int id;

    private int prijavaAkcijaCehId;
    private int prijavaAkcijaLikId;

    private List<PrijavaUCeh> prijave;

    public void init(){
        prijave = Database.dohvatiPrijave(id, Svrha.PRIJAVE);
    }

    public void doOdbij(){
        obrisiPrijavu(prijavaAkcijaCehId, prijavaAkcijaLikId);
    }

    public void doPrihvati(){
        Lik lik = Database.dohvatiLika(prijavaAkcijaLikId, Svrha.PRIJAVE);

        if(!Database.uclaniKorisnika(prijavaAkcijaCehId, prijavaAkcijaLikId, lik.getKorisnik().getId(), Ovlast.CLAN)){
            Messages.addMessage("Došlo je do pogreške kod spremanja podataka");
        }
    }

    private void obrisiPrijavu(int cehId, int likId){
        if(!Database.obrisiPrijavu(cehId, likId)){
            Messages.addMessage("Došlo je do pogreške kod spremanja podataka");
        }
    }

    public List<PrijavaUCeh> getPrijave() {
        return prijave;
    }

    public void setPrijave(List<PrijavaUCeh> prijave) {
        this.prijave = prijave;
    }

    public int getPrijavaAkcijaCehId() {
        return prijavaAkcijaCehId;
    }

    public void setPrijavaAkcijaCehId(int prijavaAkcijaCehId) {
        this.prijavaAkcijaCehId = prijavaAkcijaCehId;
    }

    public int getPrijavaAkcijaLikId() {
        return prijavaAkcijaLikId;
    }

    public void setPrijavaAkcijaLikId(int prijavaAkcijaLikId) {
        this.prijavaAkcijaLikId = prijavaAkcijaLikId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
