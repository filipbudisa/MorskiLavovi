package com.morskilavovi.beans;

import com.morskilavovi.Database;
import com.morskilavovi.Session;
import com.morskilavovi.entities.Korisnik;
import com.morskilavovi.entities.Poruka;
import com.morskilavovi.enums.Svrha;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.List;

@ManagedBean(name = "razgovori")
@RequestScoped
public class RazgovoriBean {

    private int id;

    private List<Poruka> razgovori;

    public void init(){
        id = (int) Session.getAttribute("user");
        razgovori = Database.dohvatiPoruke(id, Svrha.RAZGOVORI);
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public List<Poruka> getRazgovori() {
        return razgovori;
    }

    public void setRazgovori(List<Poruka> razgovori) {
        this.razgovori = razgovori;
    }

}
