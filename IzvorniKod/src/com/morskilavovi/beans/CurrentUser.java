package com.morskilavovi.beans;


import com.morskilavovi.Database;
import com.morskilavovi.Session;
import com.morskilavovi.entities.Ceh;
import com.morskilavovi.entities.Korisnik;
import com.morskilavovi.entities.Lik;
import com.morskilavovi.entities.Poruka;
import com.morskilavovi.enums.Svrha;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import java.util.List;

@ManagedBean
@RequestScoped
public class CurrentUser {

	private int id;
	private int neprRazgovori;
	private Korisnik korisnik;
	private List<Ceh> cehovi;
	private List<Lik> likovi;

	public void init(){
        id = (int) Session.getAttribute("user");
        korisnik = Database.dohvatiKorisnika(id, Svrha.CURRENTUSER);
        cehovi = Database.korisnikDohvatiCehove(id, Svrha.CURRENTUSER);
        //likovi = Database.korisnikDohvatiLikove(id, Svrha.CURRENTUSER);
        neprRazgovori = Database.dohvatiBrojRazgovoraSNepregledanimPorukama(id);
    }

	public String doLogout(){
		Session.deleteSession();
		return "/login.xhtml?faces-redirect=true";
	}

    public void setId(int id) {
        this.id = id;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public List<Ceh> getCehovi() {
        return cehovi;
    }

    public void setCehovi(List<Ceh> cehovi) {
        this.cehovi = cehovi;
    }

    public List<Lik> getLikovi() {
        return likovi;
    }

    public void setLikovi(List<Lik> likovi) {
        this.likovi = likovi;
    }

    public int getId() {
        return id;
    }

    public int getNeprRazgovori() {
        return neprRazgovori;
    }

    public void setNeprRazgovori(int neprRazgovori) {
        this.neprRazgovori = neprRazgovori;
    }
}
