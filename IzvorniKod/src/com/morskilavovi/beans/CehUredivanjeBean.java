package com.morskilavovi.beans;

import com.morskilavovi.Database;
import com.morskilavovi.Messages;
import com.morskilavovi.entities.Ceh;
import com.morskilavovi.enums.Svrha;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "cehuredivanje")
@ViewScoped
public class CehUredivanjeBean {

    private int id;

    private Ceh ceh;

    private int cehBrisId;
    private String opis;
    private String postignuca;
    // private Image logo;
    // private Image banner;

    public void init(){
        ceh = Database.dohvatiCeh(id, Svrha.CEHUREDIVANJE);
    }

    public void doPohrani(){

        if(!Database.azurirajCeh(id, opis, postignuca)){
            Messages.addMessage("Došlo je do pogreške u spremanju podataka");
        } else {
            Messages.addMessage("Podatci ceha ažurirani");
        }

    }

    public void doCehObrisi(){
        return;
        /*if(!Database.obrisiCeh(cehBrisId)){
            Messages.addMessage("Došlo je do pogreške kod brisanja ceha");
        } else {
            Messages.addMessage("Ceh je uspješno izbrisan");
        }*/
    }

    public Ceh getCeh() {
        return ceh;
    }

    public void setCeh(Ceh ceh) {
        this.ceh = ceh;
    }

    public int getCehBrisId() {
        return cehBrisId;
    }

    public void setCehBrisId(int cehBrisId) {
        this.cehBrisId = cehBrisId;
    }

    public String getOpis() {
        return "";
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getPostignuca() {
        return postignuca;
    }

    public void setPostignuca(String postignuca) {
        this.postignuca = postignuca;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
