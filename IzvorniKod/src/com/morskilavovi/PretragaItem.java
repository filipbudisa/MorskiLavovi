package com.morskilavovi;

import com.morskilavovi.entities.Ceh;
import com.morskilavovi.entities.Korisnik;
import com.morskilavovi.enums.ItemPretraga;

public class PretragaItem {

    private int tip;
    private ItemPretraga tipPr;
    private Korisnik korisnik;
    private Ceh ceh;

    public PretragaItem(ItemPretraga tip, Korisnik korisnik, Ceh ceh){
        this.tipPr = tip;
        this.korisnik = korisnik;
        this.ceh = ceh;
        this.tip = tipPr.ordinal();
    }

    public int getTip(){
        return tip;
    }

    public void setTip(int tip){
        this.tip = tip;
    }

    public ItemPretraga getTipPr(){
        return tipPr;
    }

    public void setTipPr(ItemPretraga tipPr){
        this.tipPr = tipPr;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public Ceh getCeh() {
        return ceh;
    }

    public void setCeh(Ceh ceh) {
        this.ceh = ceh;
    }
}
