package com.morskilavovi;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Utils {
	private static final Lock md5Lock = new ReentrantLock();

	private static MessageDigest md5 = null;

	public static String md5(String string){
		if(md5 == null) initMd5();

		md5Lock.lock();
		md5.update(string.getBytes());
		byte[] digest = md5.digest();
		md5Lock.unlock();

		return DatatypeConverter.printHexBinary(digest);
	}

	private static void initMd5(){
		try{
			md5 = MessageDigest.getInstance("MD5");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
