package com.morskilavovi;

import com.morskilavovi.entities.*;
import com.morskilavovi.enums.Ovlast;
import com.morskilavovi.enums.Svrha;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.time.LocalDateTime;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Arrays;

public class Database {
	private static DataSource pgDS;

	private static Connection pgCon = null;

	private static Connection getCon() {
		if(pgCon == null) reconnect();
		return pgCon;
	}

	private static int tries = 0;

	/**
	 * Uspostavi vezu s bazom. Ako vrati false, znači da je nekva greška
	 * @return True ako je uspješno, false ako nije
	 */
	private static boolean reconnect() {
		if(tries++ == 1){
			tries = 0;
			return false;
		}

		try{
			InitialContext cxt = new InitialContext();
			DataSource pgDS = (DataSource) cxt.lookup( "java:/comp/env/jdbc/postgresq" );

			if(pgDS != null){
				pgCon = pgDS.getConnection();

				return pgCon != null;
			}else{
				System.out.println("data source not found");
				return false;
			}
		}catch(Exception e){
			e.printStackTrace();
		}

		return true;
	}

	/**
	 * Pokuša ulogirati korisnika
	 * @return ID korisnika, -1 ako login nije uspješan
	 */
	public static int tryLogin(String user, String pass) {
		//if(user.equals("a") && pass.equals("a")) return 1;

		String passMd5 = Utils.md5(pass);

		Connection con = getCon();

		try{
			PreparedStatement stat = con.prepareStatement("SELECT userID FROM korisnik WHERE nadimak = ? AND lozinka = ?");

			stat.setString(1, user);
			stat.setString(2, passMd5);

			ResultSet rs = stat.executeQuery();

			if(rs.next()){
				return rs.getInt("userID");
			}else{
				return -1;
			}
		}catch(Exception e){
			if(reconnect()) return tryLogin(user, pass);
		}

		 return -1;
	}

	/**
	 * Pokuša registrirati i ulogirati novog korisnika
	 * @return ID novog korisnika, -1 ako je ime zauzeto, -2 ako je email zauzet
	 */
	public static int tryRegister(String username, String email, String password){
		Connection con = getCon();

		try{
			PreparedStatement stat = con.prepareStatement("SELECT userid	 FROM korisnik WHERE nadimak = ?");

			stat.setString(1, username);
			ResultSet rs = stat.executeQuery();

			if(rs.next()) return -1;

			stat = con.prepareStatement("SELECT userid FROM korisnik WHERE email = ?");

			stat.setString(1, email);
			rs = stat.executeQuery();

			if(rs.next()) return -2;

			String passMd5 = Utils.md5(password);
			stat = con.prepareStatement("INSERT INTO korisnik (nadimak, email, lozinka) VALUES(?, ?, ?)");

			stat.setString(1, username);
			stat.setString(2, email);
			stat.setString(3, passMd5);
			stat.execute();


			stat = con.prepareStatement("SELECT userid FROM korisnik WHERE email = ?");

			stat.setString(1, email);
			rs = stat.executeQuery();

			rs.next();


			return rs.getInt("userID");


		}catch(Exception e){
			if(reconnect()) return tryRegister(username, email, password);
		}

		return -3;
	}


	/**
	 * Dohvati sve igre
	 */
	public static List<Igra> dohvatiIgre(Svrha svrha){
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT igraid, imeigre FROM igra");
			ResultSet rs = pstat.executeQuery();

			ArrayList<Igra> igre = new ArrayList<>();
			while (rs.next()) {
				String imeIgre = rs.getString("imeigre");
				int igraId = rs.getInt("igraid");

				Igra igra = new Igra(igraId, imeIgre, svrha);
				if (!igre.contains(igra)) {
					igre.add(igra);
				}
			}

			return igre;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dohvatiIgre(svrha);
			}
		}

		return new ArrayList<>();
	}

	/**
	 * Vraca igraID za igru zadanog naziva
	 * @param nazivIgre
	 * @return
	 */
	public static int dohvatiIdIgre(String nazivIgre) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT igraid FROM igra WHERE imeigre = ?");
			pstat.setString(1, nazivIgre);
			ResultSet rs = pstat.executeQuery(); rs.next();

			int igraId = rs.getInt("igraid");

			return igraId;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dohvatiIdIgre(nazivIgre);
			}
		}

		return -1;
	}

	/**
	 * Dohvati sve klase za zadanu igru
	 */
	public static List<Klasa> dohvatiKlase(int igraId, Svrha svrha){
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT imeklase, klasaid FROM klasa WHERE igraid = ?");
			pstat.setInt(1, igraId);
			ResultSet rs = pstat.executeQuery();

			ArrayList<Klasa> klase = new ArrayList<>();
			while (rs.next()) {
				int klasaId = rs.getInt("klasaid");
				String ik = rs.getString("imeklase");
				Klasa k = new Klasa(klasaId, igraId, ik, svrha);
				if (!klase.contains(k)) {
					klase.add(k);
				}
			}

			return klase;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dohvatiKlase(igraId, svrha);
			}
		}

		return new ArrayList<>();
	}

	/**
	 * @return True ako je uspješno, false ako igra već postoji
	 */
	public static boolean dodajIgru(String igraIme){
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT igraid FROM igra WHERE imeigre = ?");
			pstat.setString(1, igraIme);
			ResultSet rs = pstat.executeQuery();

			if (rs.next()) {
				return false;
			}

			pstat = con.prepareStatement("INSERT INTO igra (imeigre) VALUES (?)");
			pstat.setString(1, igraIme);
			pstat.execute();

			return true;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dodajIgru(igraIme);
			}
		}

		return false;
	}

	/**
	 * Dodaj klasu za igru
	 * @return True ako je uspješno, false ako ta klasa za tu igru već postoji
	 */
	public static boolean dodajKlasu(String igra, String klasa){
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT klasaID FROM klasa WHERE imeKlase = ? AND igraID = ?");
			pstat.setString(1, klasa);
			int igraId = dohvatiIdIgre(igra);
			pstat.setInt(2, igraId);
			ResultSet rs = pstat.executeQuery();

			if (rs.next()) {
				return false;
			}

			pstat = con.prepareStatement("INSERT INTO klasa (igraID, imeKlase) VALUES (?, ?)");
			pstat.setInt(1, igraId);
			pstat.setString(2, klasa);
			pstat.execute();

			return true;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dodajKlasu(igra, klasa);
			}
		}

		return false;
	}

	/**
	 * Vraca Lik za zadani likId
	 * @param likId
	 * @return
	 */
	public static Lik dohvatiLika(int likId, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT vjestine, imelik, userID, klasa, igra, razina FROM lik WHERE likID = ?");
			pstat.setInt(1, likId);
			ResultSet rs = pstat.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int userId = rs.getInt("userID");
			int kid = rs.getInt("klasa");
			int iid = rs.getInt("igra");
			int raz = rs.getInt("razina");
			String imelik = rs.getString("imelik");
			String vjstineString = rs.getString("vjestine");
			List<String> vjestine;
			if(vjstineString != null && !vjstineString.equals("")) vjestine = Arrays.asList(vjstineString.split(","));
			else vjestine = new ArrayList<>();

			Lik l = new Lik(likId, userId, imelik, iid, kid, raz, vjestine, svrha);

			return l;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dohvatiLika(likId, svrha);
			}
		}

		return null;
	}

	/**
	 * Dohvati likove korisnika
	 */
	public static ArrayList<Lik> korisnikDohvatiLikove(int korisnikId, Svrha svrha){
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT likID FROM lik WHERE userID = ?");
			pstat.setInt(1, korisnikId);
			ResultSet rs = pstat.executeQuery();

			ArrayList<Lik> likovi = new ArrayList<>();
			while (rs.next()) {
				int likId = rs.getInt("likID");
				Lik l = dohvatiLika(likId, svrha);
				if (!likovi.contains(l)) {
					likovi.add(l);
				}
			}

			return likovi;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return korisnikDohvatiLikove(korisnikId, svrha);
			}
		}

		return new ArrayList<>();
	}

	/**
	 * Dodaj lika
	 * @return True ako je uspjesno, false ako taj lik za tog korisnika vec postoji
	 */
	/*public static boolean korisnikDodajLika(Korisnik korisnik, Klasa klasa, String ime){
		Connection con = getCon();

		try {
			Lik lik = new Lik(korisnik, klasa);
			ArrayList<Lik> likovi = korisnikDohvatiLikove(korisnik.getId());
			if (likovi.contains(lik)) {
				return false;
			}

			PreparedStatement pstat = con.prepareStatement("INSERT INTO lik (userID, imeLik, igra, klasa) VALUES (?, ?, ?, ?)");
			pstat.setInt(1, korisnik.getId());
			pstat.setString(2, ime);
			pstat.setString(3, klasa.getIgra());
			pstat.setString(4, klasa.getNaziv());
			pstat.execute();


		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return korisnikDodajLika(korisnik, klasa, ime);
			}
		}

		return false;
	}*/

	public static boolean stvoriCeh(String naziv, int igraId, int likId){
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT cehID FROM ceh WHERE imeCeh = ? AND igraID = ?");
			pstat.setString(1, naziv);
			pstat.setInt(2, igraId);
			ResultSet rs = pstat.executeQuery();

			if (rs.next()) {
				return false;
			}


			pstat = con.prepareStatement("INSERT INTO ceh (imeCeh, igraID) VALUES (?, ?)");
			pstat.setString(1, naziv);
			pstat.setInt(2, igraId);
			pstat.execute();

			pstat = con.prepareStatement("SELECT cehID FROM ceh WHERE imeCeh = ? AND igraID = ?");
			pstat.setString(1, naziv);
			pstat.setInt(2, igraId);
			ResultSet rsc = pstat.executeQuery(); rs.next();
			int cehId = rsc.getInt("cehID");

			pstat = con.prepareStatement("SELECT userID FROM lik WHERE likID = ?");
			pstat.setInt(1, likId);
			ResultSet rsu = pstat.executeQuery(); rs.next();
			int korisnikId = rsu.getInt("userID");

			pstat = con.prepareStatement("INSERT INTO clanstvo (cehID, likID, userID, uloga) VALUES (?, ?, ?, ?)");
			pstat.setInt(1, cehId);
			pstat.setInt(2, likId);
			pstat.setInt(3, korisnikId);
			pstat.setString(3, "vođa");
			pstat.execute();

			return true;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return stvoriCeh(naziv, igraId, likId);
			}
		}

		return false;
	}

	/**
	 * Unos prijave u bazu podataka
	 * @return true ako je uspjesno, inace false
	 */
	/*public static boolean prijavaUCeh(PrijavaUCeh prijava){
		Connection con = getCon();

		try {
			int cehId = prijava.getCeh().getId();
			int likId = dohvatiLikID(prijava.getLik());
			int userId = prijava.getLik().getKorisnik().getId();
			String sadrzaj = prijava.getSadrzaj();

			PreparedStatement pstat = con.prepareStatement("SELECT cehID FROM prijave WHERE cehID = ? AND likID = ?");
			pstat.setInt(1, cehId);
			pstat.setInt(2, likId);
			ResultSet rs = pstat.executeQuery();

			if (rs.next()) {
				return false;
			}


			pstat = con.prepareStatement("INSERT INTO prijave (cehID, likID, userID, sadrzaj) VALUES (?, ?, ?, ?)");
			pstat.setInt(1, cehId);
			pstat.setInt(2, likId);
			pstat.setInt(3, userId);
			pstat.setString(4, sadrzaj);
			pstat.execute();

			return true;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return prijavaUCeh(prijava);
			}
		}

		return false;
	}*/

	/**
	 * Vraca ceh kojem zadani lik pripada
	 */
	public static Ceh dohvatiCeh(int cehId, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT imeCeh, igraid, opis, glasanje FROM ceh WHERE cehID = ?");
			pstat.setInt(1, cehId);
			ResultSet rsc = pstat.executeQuery(); rsc.next();
			String naziv = rsc.getString("imeceh");
			String opis = rsc.getString("opis");
			int igraid = rsc.getInt("igraid");
			boolean glasanje = rsc.getBoolean("glasanje");

			Ceh ceh = new Ceh(cehId, naziv, igraid, opis, new ArrayList<Cilj>(), glasanje, svrha);

			return ceh;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dohvatiCeh(cehId, svrha);
			}
		}

		return null;
	}

	/**
	 * Dohvati sve prijave za zadani ceh
	 */
	public static List<PrijavaUCeh> dohvatiPrijave(int cehId, Svrha svrha){
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT likID, userID, sadrzaj FROM prijave WHERE cehID = ?");
			pstat.setInt(1, cehId);
			ResultSet rs = pstat.executeQuery();

			ArrayList<PrijavaUCeh> prijave = new ArrayList<>();
			while (rs.next()) {
				int likId = rs.getInt("likID");
				int korisnikId = rs.getInt("userID");
				String sadrzaj = rs.getString("sadrzaj");

				PrijavaUCeh prijava = new PrijavaUCeh(likId, cehId, korisnikId, sadrzaj, svrha);
				if (!prijave.contains(prijava)) {
					prijave.add(prijava);
				}
			}

			return prijave;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dohvatiPrijave(cehId, svrha);
			}
		}

		return new ArrayList<>();
	}

	/**
	 * Ukloniti prijavu iz relacije prijave
	 * Ako je odgovor true, dodati clana cehu
	 */
	/*public static void odgovorNaPrijavu(PrijavaUCeh prijava, boolean odgovor){
		Connection con = getCon();

		try {
			int cehId = prijava.getCehId();
			int likId = prijava.getLikId();
			int userId = prijava.getLik().getKorisnik().getId();
			String tekst = prijava.getSadrzaj();

			PreparedStatement pstat = con.prepareStatement("DELETE FROM prijave WHERE cehId = ? AND likID = ?");
			pstat.setInt(1, cehId);
			pstat.setInt(2, likId);
			pstat.execute();

			if (!odgovor) {
				//ako je odgovor false, ne dodajemo clana cehu
				return;
			}

			pstat = con.prepareStatement("INSERT INTO clanstvo (cehID, likID, userID) VALUES (?, ?, ?)");
			pstat.setInt(1, cehId);
			pstat.setInt(2, likId);
			pstat.setInt(3, userId);
			pstat.execute();

		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				odgovorNaPrijavu(prijava, odgovor);
			}
		}
	}*/

	/**
	 * Vraca Korisnik za zadani userID
	 */
	public static Korisnik dohvatiKorisnika(int idKorisnika, Svrha svrha){
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT email, nadimak, about FROM korisnik WHERE userID = ?");
			pstat.setInt(1, idKorisnika);
			ResultSet rs = pstat.executeQuery();

			rs.next();

			String ime = rs.getString("nadimak");
			String email = rs.getString("email");
			String about = rs.getString("about");

			Korisnik korisnik = new Korisnik(idKorisnika, email, ime, about, idKorisnika == 2, svrha);
			return korisnik;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse(); e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dohvatiKorisnika(idKorisnika, svrha);
			}
		}

		return null;
	}

	/**
	 * @param korisnik Trenutni korisnik
	 * @return Lista korisnika s kojima korisnik ima razgovore
	 */
	public static List<Korisnik> dohvatiRazgovore(Korisnik korisnik, Svrha svrha){
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT posiljatelj FROM poruka WHERE primatelj = ?");
			pstat.setInt(1, korisnik.getId());
			ResultSet rs = pstat.executeQuery();

			ArrayList<Korisnik> sugovornici = new ArrayList<>();
			while (rs.next()) {
				int kid = rs.getInt("posiljatelj");
				Korisnik k = dohvatiKorisnika(kid, svrha);
				if (!sugovornici.contains(k)) {
					sugovornici.add(k);
				}
			}

			pstat = con.prepareStatement("SELECT primatelj FROM poruka WHERE posiljatelj = ?");
			pstat.setInt(1, korisnik.getId());
			rs = pstat.executeQuery();

			while (rs.next()) {
				int kid = rs.getInt("primatelj");
				Korisnik k = dohvatiKorisnika(kid, svrha);
				if (!sugovornici.contains(k)) {
					sugovornici.add(k);
				}
			}

			return sugovornici;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dohvatiRazgovore(korisnik, svrha);
			}
		}

		return new ArrayList<>();
	}

	/**
	 * Dohvaća listu poruka između dva korisnika
	 * @param trenutniId userID trenutnog korisnika
	 * @param drugiId userID drugog korisnika
	 * @param limit Broj poruka
	 * @param pocetak Pocevsi od ove u redu
	 */
	public static List<Poruka> dohvatiRazgovor(int trenutniId, int drugiId, int limit, int pocetak, Svrha svrha){
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT * FROM poruka "
					+ "WHERE (posiljatelj = ? AND primatelj = ? ) OR (primatelj = ? AND posiljatelj = ? ) "
					+ "ORDER BY porukaid DESC");
			pstat.setInt(1, trenutniId);
			pstat.setInt(2, drugiId);
			pstat.setInt(3, trenutniId);
			pstat.setInt(4, drugiId);
			ResultSet rs = pstat.executeQuery();

			ArrayList<Poruka> poruke = new ArrayList<>();
			while (rs.next()) {
				int porukaId = rs.getInt("porukaID");
				int pos = rs.getInt("posiljatelj");
				int prim = rs.getInt("primatelj");
				String sadrzaj = rs.getString("sadrzaj");
				LocalDateTime vrijeme = rs.getTimestamp("vrijeme").toLocalDateTime();

				Korisnik trenutni = dohvatiKorisnika(trenutniId, svrha);
				Korisnik drugi = dohvatiKorisnika(drugiId, svrha);
				if (pos == trenutniId) {
					//posiljatelj je trenutni korisnik
					Poruka p = new Poruka(0, trenutni.getId(), drugi.getId(), sadrzaj, "vrijeme", false, svrha);
					poruke.add(p);
				} else {
					//posiljatelj je drugi korisnik
					Poruka p = new Poruka(0, drugi.getId(), trenutni.getId(), sadrzaj, "vrijeme", false, svrha);
					poruke.add(p);

					/*pstat = con.prepareStatement("UPDATE poruka SET pregledano = ? WHERE porukaID = ?");
					pstat.setBoolean(1, true);
					pstat.setInt(2, porukaId);
					pstat.executeQuery();*/
				}
			}

			return poruke;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dohvatiRazgovor(trenutniId, drugiId, limit, pocetak, svrha);
			}
		}

		return new ArrayList<>();
	}

	/**
	 * Dodaje poruku u bazu podataka
	 */
	public static boolean dodajPoruku(int posiljateljId, int primateljId, String tekst){
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("INSERT INTO poruka (posiljatelj, primatelj, sadrzaj, vrijeme, pregledano) VALUES (?, ?, ?, ?, ?)");
			pstat.setInt(1, posiljateljId);
			pstat.setInt(2, primateljId);
			pstat.setString(3, tekst);
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			pstat.setTimestamp(4, ts);
			pstat.setBoolean(5, false);
			boolean r = pstat.execute();

			return r;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				dodajPoruku(posiljateljId, primateljId, tekst);
			}
		}
		return false;
	}






	/**
	 *Vraca listu svih clanstava koja korisnik ima
	 */
	public static List<Clanstvo> dohvatiClanstva(int korisnikId, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT clanstvoID, cehID, likID, uloga "
					+ "FROM clanstvo WHERE userID = ?");
			pstat.setInt(1, korisnikId);
			ResultSet rs = pstat.executeQuery();

			ArrayList<Clanstvo> clanstva = new ArrayList<>();
			while (rs.next()) {
				int clanstvoId = rs.getInt("clanstvoID");
				int cehId = rs.getInt("cehID");
				int likId = rs.getInt("likID");
				String uloga = rs.getString("uloga");

				if (uloga.equalsIgnoreCase("vođa") || uloga.equalsIgnoreCase("voda")) {
					Clanstvo c = new Clanstvo(clanstvoId, cehId, likId, korisnikId, Ovlast.VODA, svrha);
					if (!(clanstva.contains(c))) {
						clanstva.add(c);
					}
				} else if (uloga.equalsIgnoreCase("koordinator")) {
					Clanstvo c = new Clanstvo(clanstvoId, cehId, likId, korisnikId, Ovlast.KOORDINATOR, svrha);
					if (!(clanstva.contains(c))) {
						clanstva.add(c);
					}
				} else if (uloga.equalsIgnoreCase("član") || uloga.equalsIgnoreCase("clan")) {
					Clanstvo c = new Clanstvo(clanstvoId, cehId, likId, korisnikId, Ovlast.CLAN, svrha);
					if (!(clanstva.contains(c))) {
						clanstva.add(c);
					}
				}

			}

			return clanstva;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dohvatiClanstva(korisnikId, svrha);
			}
		}

		return new ArrayList<>();
	}

	//Podcilj(int id, int ciljId, int cehId, String ime, boolean postignut, LocalDate datum)
	/**
	 *Vraca listu podciljeva pridruzenih zadanom cilju
	 */
	public static List<Podcilj> dohvatiPodciljeve(int ciljId, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT podciljID, cehID, imePodcilj, datum, postignut FROM podcilj WHERE ciljID = ?");
			pstat.setInt(1, ciljId);
			ResultSet rs = pstat.executeQuery();

			ArrayList<Podcilj> podciljevi = new ArrayList<>();
			while (rs.next()) {
				int podciljId = rs.getInt("podciljID");
				int cehId = rs.getInt("cehID");
				String imePodcilj = rs.getString("imePodcilj");
				Date datum = rs.getDate("datum");
				LocalDate datumL = datum.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				boolean postignut = rs.getBoolean("postignut");

				Podcilj p = new Podcilj(podciljId, ciljId, cehId, imePodcilj, postignut, "");
				if (!podciljevi.contains(p)) {
					podciljevi.add(p);
				}
			}

			return podciljevi;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dohvatiPodciljeve(ciljId, svrha);
			}
		}

		return null;
	}

	/**
	 *Vraca listu likove koji sudjeluju u nekom cilju
	 */
	public static List<Lik> ciljDohvatiLikove(int ciljId, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT likID FROM sudjelovanje WHERE ciljID = ?");
			pstat.setInt(1, ciljId);
			ResultSet rs = pstat.executeQuery();

			ArrayList<Lik> likovi = new ArrayList<>();
			while (rs.next()) {
				int likId = rs.getInt("likID");
				Lik lik = dohvatiLika(likId, svrha);

				if (!likovi.contains(lik)) {
					likovi.add(lik);
				}

			}

			return likovi;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return ciljDohvatiLikove(ciljId, svrha);
			}
		}

		return null;
	}

	//Cilj(int id, int cehId, String ime, boolean postignut, LocalDate datum)
	/**
	 * Vraca listu ciljeva u kojima zadani lik sudjeluje
	 */
	public static List<Cilj> likDohvatiCiljeve(int likId, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT ciljID FROM sudjelovanje WHERE likID = ?");
			pstat.setInt(1, likId);
			ResultSet rs = pstat.executeQuery();

			ArrayList<Cilj> ciljevi = new ArrayList<>();
			while (rs.next()) {
				int ciljId = rs.getInt("ciljID");

				pstat = con.prepareStatement("SELECT cehID, FROM cilj WHERE ciljID = ?");
				pstat.setInt(1, ciljId);
				ResultSet rsc = pstat.executeQuery();

				rs.next();

				int cehId = rsc.getInt("cehID");
				String imeCilj = rsc.getString("imeCilj");
				Date datum = rsc.getDate("datum");
				LocalDate datumL = datum.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				boolean postignut = rsc.getBoolean("postignut");

				Cilj c = new Cilj(ciljId, cehId, imeCilj, postignut, "datum", svrha);
				if (!ciljevi.contains(c)) {
					ciljevi.add(c);
				}
			}

			return ciljevi;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return likDohvatiCiljeve(likId, svrha);
			}
		}

		return null;
	}

	/**
	 * Vraca listu svih ciljeva koje je zadani lik postignuo
	 */
	public static List<Cilj> likDohvatiPostignuca(int likId, Svrha svrha) {
		ArrayList<Cilj> postignutiCiljevi = new ArrayList<>();

		List<Cilj> ciljevi = likDohvatiCiljeve(likId, svrha);
		for (Cilj cilj : ciljevi) {
			if (cilj.isPostignut()) {
				postignutiCiljevi.add(cilj);
			}
		}
		return postignutiCiljevi;
	}

	/**
	 *Vraca vođu ceha
	 */
	/*public static Korisnik cehDohvatiVodu(int cehId, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT userID FROM clanstvo WHERE uloga = ? AND cehid = ?");
			pstat.setString(1, "vođa");
			pstat.setInt(2, cehId);
			ResultSet rs = pstat.executeQuery();

			int userId = rs.getInt("userID");

			return dohvatiKorisnika(userId, svrha);
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return cehDohvatiVodu(cehId, svrha);
			}
		}
		return null;
	}*/

	/**
	 *Vraca koordinatore u cehu
	 */
	public static List<Korisnik> cehDohvatiKoordinatore(int cehId, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT userID FROM clanstvo WHERE uloga = ? AND cehid = ?");
			pstat.setString(1, "koordinator");
			pstat.setInt(2, cehId);
			ResultSet rs = pstat.executeQuery();

			ArrayList<Korisnik> koordinatori = new ArrayList<>();
			while (rs.next()) {
				int userId = rs.getInt("userID");

				pstat = con.prepareStatement("SELECT nadimak, email FROM korisnik WHERE userID = ?");
				pstat.setInt(1, userId);
				ResultSet rsk = pstat.executeQuery();
				rsk.next();
				String ime = rsk.getString("nadimak");
				String email = rsk.getString("email");

				Korisnik k = new Korisnik(userId, ime, email, "", false, svrha);
				if (!(koordinatori.contains(k))) {
					koordinatori.add(k);
				}

			}

			return koordinatori;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return cehDohvatiKoordinatore(cehId, svrha);
			}
		}

		return new ArrayList<>();
	}

	/**
	 *Vraca obične članove u cehu
	 */
	/*public static List<Korisnik> cehDohvatiClanove(int cehId) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT userID FROM clanstvo WHERE uloga = ?");
			pstat.setString(1, "član");
			ResultSet rs = pstat.executeQuery();

			ArrayList<Korisnik> clanovi = new ArrayList<>();
			while (rs.next()) {
				int userId = rs.getInt("userID");

				pstat = con.prepareStatement("SELECT nadimak, email FROM korisnik WHERE userID = ?");
				pstat.setInt(1, userId);
				ResultSet rsk = pstat.executeQuery();
				String ime = rsk.getString("nadimak");
				String email = rsk.getString("email");

				Korisnik k = new Korisnik(userId, ime, email);
				if (!(clanovi.contains(k))) {
					clanovi.add(k);
				}

			}

			return clanovi;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return cehDohvatiClanove(cehId);
			}
		}

		return new ArrayList<>();
	}*/

	//Cilj(int id, int cehId, String ime, boolean postignut, LocalDate datum)
	/**
	 *Vraca ciljeve zadanog ceha
	 */
	public static List<Cilj> cehDohvatiCiljeve(int cehId, Svrha svrha) {
		Connection con = getCon();

		return new ArrayList<Cilj>();

		/*try {
			PreparedStatement pstat = con.prepareStatement("SELECT ciljID FROM sudjelovanje WHERE cehID = ?");
			pstat.setInt(1, cehId);
			ResultSet rs = pstat.executeQuery();

			ArrayList<Cilj> ciljevi = new ArrayList<>();
			while (rs.next()) {
				int ciljId = rs.getInt("ciljID");

				pstat = con.prepareStatement("SELECT imeCilj, datum, postignut FROM cilj WHERE ciljID = ?");
				pstat.setInt(1, ciljId);
				ResultSet rsc = pstat.executeQuery();

				String imeCilj = rsc.getString("imeCilj");
				Date datum = rsc.getDate("datum");
				LocalDate datumL = datum.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				boolean postignut = rsc.getBoolean("postignut");

				Cilj c = new Cilj(ciljId, cehId, imeCilj, postignut, "vijeme", svrha);
				if (!ciljevi.contains(c)) {
					ciljevi.add(c);
				}
			}

			return ciljevi;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return cehDohvatiCiljeve(cehId, svrha);
			}
		} */

		//return null;
	}

	//Dogadaj(int id, int cehId, String ime, String opis, LocalDateTime vrijeme)
	/**
	 *Vraca dogadaje zadanog ceha
	 */
	public static List<Dogadaj> cehDohvatiDogadaje(int cehId, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT dogID FROM dogadaj WHERE cehID = ?");
			pstat.setInt(1, cehId);
			ResultSet rs = pstat.executeQuery();

			ArrayList<Dogadaj> dogadaji = new ArrayList<>();
			while (rs.next()) {
				int dogId = rs.getInt("dogID");

				pstat = con.prepareStatement("SELECT imeDogadaj, opis, vrijeme FROM dogadaj WHERE dogID = ?");
				pstat.setInt(1, dogId);
				ResultSet rsd = pstat.executeQuery();
				rsd.next();

				String imeDog = rsd.getString("imeDogadaj");
				String opis = rsd.getString("opis");
				Timestamp vrijeme = rsd.getTimestamp("vrijeme");
				LocalDateTime vrijemeL = vrijeme.toLocalDateTime();

				Dogadaj d = new Dogadaj(dogId, cehId, imeDog, opis, "vrijeme", Svrha.CEHDOG);
				if (!dogadaji.contains(d)) {
					dogadaji.add(d);
				}
			}

			return dogadaji;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return cehDohvatiDogadaje(cehId, svrha);
			}
		}

		return null;
	}

	//Obavijest(int id, int cehId, int autorId, String naslov, String sadrzaj, LocalDateTime vrijeme)
	/**
	 *Vraca obavijesti zadanog ceha
	 */
	public static List<Obavijest> cehDohvatiObavijesti(int cehId, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT obavijestID FROM obavijest WHERE cehID = ?");
			pstat.setInt(1, cehId);
			ResultSet rs = pstat.executeQuery();

			ArrayList<Obavijest> obavijesti = new ArrayList<>();
			while (rs.next()) {
				int obavijestId = rs.getInt("obavijestID");

				pstat = con.prepareStatement("SELECT autor, naslov, sadrzaj, vrijeme FROM obavijest WHERE obavijestID = ?");
				pstat.setInt(1, obavijestId);
				ResultSet rso = pstat.executeQuery();
				rso.next();
				int autorId = rso.getInt("autor");
				String naslov = rso.getString("naslov");
				String sadrzaj = rso.getString("sadrzaj");
				Timestamp vrijeme = rso.getTimestamp("vrijeme");

				Obavijest o = new Obavijest(obavijestId, cehId, autorId, naslov, sadrzaj, svrha);
				if (!obavijesti.contains(o)) {
					obavijesti.add(o);
				}
			}

			return obavijesti;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return cehDohvatiObavijesti(cehId, svrha);
			}
		}

		return null;
	}

	/**
	 * Vraca lika koji je napisao zadanu obavijest
	 */
	/*public static Lik obavijestDohvatiLik(int autorId) {
		Connection con = getCon();

		try {
			Lik lik = dohvatiLika(autorId);
			return lik;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return obavijestDohvatiLik(autorId);
			}
		}

		return null;
	}*/

	/**
	 *Vraca igru zadanog ceha
	 */
	public static Igra cehDohvatiIgru(int igraId, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT imeIgre FROM igra WHERE igraID = ?");
			pstat.setInt(1, igraId);
			ResultSet rs = pstat.executeQuery();
			rs.next();

			String imeIgre = rs.getString("imeIgre");

			Igra i = new Igra(igraId, imeIgre, svrha);

			return i;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return cehDohvatiIgru(igraId, svrha);
			}
		}

		return null;
	}

	/**
	 * Vraca sve cehove u koje je korisnik uclanjen
	 */
	public static List<Ceh> korisnikDohvatiCehove(int korisnikId, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT cehID FROM clanstvo WHERE userID = ?");
			pstat.setInt(1, korisnikId);
			ResultSet rs = pstat.executeQuery();

			ArrayList<Ceh> cehovi = new ArrayList<>();
			while (rs.next()) {
				int cehId = rs.getInt("cehID");
				Ceh ceh = dohvatiCeh(cehId, svrha);

				if (!cehovi.contains(ceh)) {
					cehovi.add(ceh);
				}
			}

			return cehovi;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return korisnikDohvatiCehove(korisnikId, svrha);
			}
		}

		return null;
	}

	/**
	 * Vraca klasu kojoj lik pripada
	 */
	public static Klasa likDohvatiKlasu(int klasaId, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT igraID, klasaID, imeKlase FROM klasa WHERE klasaID = ?");
			pstat.setInt(1, klasaId);
			ResultSet rs = pstat.executeQuery(); rs.next();
			int igraId = rs.getInt("igraID");
			String imeKlase = rs.getString("imeKlase");

			pstat = con.prepareStatement("SELECT imeIgre, igraID FROM igra WHERE igraID = ?");
			pstat.setInt(1, igraId);
			rs = pstat.executeQuery(); rs.next();
			String imeIgre = rs.getString("imeIgre");

			Klasa klasa = new Klasa(klasaId, igraId, imeKlase, svrha);

			return klasa;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return likDohvatiKlasu(klasaId, svrha);
			}
		}

		return null;
	}

	/**
	 * Vraca ceh kojem zadani lik pripada
	 */
	public static Ceh likDohvatiCeh(int likId, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT cehID FROM clanstvo WHERE likID = ?");
			pstat.setInt(1, likId);
			ResultSet rs = pstat.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int cehId = rs.getInt("cehID");
			Ceh ceh = dohvatiCeh(cehId, svrha);

			return ceh;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return likDohvatiCeh(likId, svrha);
			}
		}

		return null;
	}

	/**
	 *Vraca listu korisnika koji sadrze "tekst" u nadimku
	 */
	public static ArrayList<Korisnik> pronadiKorisnike(String tekst, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT userID FROM korisnik WHERE lower(nadimak) LIKE lower('%" + tekst + "%')");
			ResultSet rs = pstat.executeQuery();

			ArrayList<Korisnik> korisnici = new ArrayList<>();
			while (rs.next()) {
				int userId = rs.getInt("userID");
				Korisnik k = dohvatiKorisnika(userId, svrha);
				if (!(korisnici.contains(k))) {
					korisnici.add(k);
				}
			}

			return korisnici;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return pronadiKorisnike(tekst, svrha);
			}
		}
		return new ArrayList<>();
	}

	/**
	 *Vraca listu cehova koji sadrze "tekst" u imenu
	 */
	public static ArrayList<Ceh> pronadiCehove(String tekst, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT cehID, imeCeh FROM ceh WHERE lower(imeCeh) LIKE lower('%" + tekst + "%')");
			ResultSet rs = pstat.executeQuery();

			ArrayList<Ceh> cehovi = new ArrayList<>();
			while (rs.next()) {
				int cehId = rs.getInt("cehID");
				Ceh ceh = dohvatiCeh(cehId, svrha);
				if (!cehovi.contains(ceh)) {
					cehovi.add(ceh);
				}
			}

			return cehovi;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return pronadiCehove(tekst, svrha);
			}
		}
		return new ArrayList<>();
	}


	/*public static Lik dohvatiLika(int userId, int cehId, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT likID FROM clanstvo WHERE userID = ? AND cehID = ?");
			pstat.setInt(1, userId);
			pstat.setInt(1, cehId);
			ResultSet rs = pstat.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int likId = rs.getInt("likID");
			Lik lik = dohvatiLika(likId, svrha);

			return lik;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dohvatiLika(userId, cehId, svrha);
			}
		}

		return null;
	}*/

	public static boolean obrisiLik(int likId) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("DELETE FROM lik WHERE likID = ?");
			pstat.setInt(1, likId);
			boolean r =pstat.execute();

			return r;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return obrisiLik(likId);
			}
		}

		return false;
	}

	public static boolean obrisiIgru(int igraId) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("DELETE FROM igra WHERE igraId = ?");
			pstat.setInt(1, igraId);
			pstat.execute();

			pstat = con.prepareStatement("SELECT imeIgra FROM igra WHERE igraID = ?");
			pstat.setInt(1, igraId);
			ResultSet rs = pstat.executeQuery();

			if (rs.next()) {
				return false;
			}

			return true;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return obrisiIgru(igraId);
			}
		}

		return false;
	}

	public static boolean obrisiKlasu(int klasaId) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("DELETE FROM klasa WHERE klasaId = ?");
			pstat.setInt(1, klasaId);
			pstat.execute();

			pstat = con.prepareStatement("SELECT imeIgra FROM klasa WHERE klasaID = ?");
			pstat.setInt(1, klasaId);
			ResultSet rs = pstat.executeQuery();

			if (rs.next()) {
				return false;
			}

			return true;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return obrisiKlasu(klasaId);
			}
		}

		return false;
	}

	public static boolean dodajKlase(int igraId, List<String> noveKlase) {
		Connection con = getCon();

		try {
			boolean r = true;
			for (String imeKlase : noveKlase) {
				PreparedStatement pstat = con.prepareStatement("INSERT INTO klasa (igraid, imeklase) VALUES (?, ?)");
				pstat.setInt(1, igraId);
				pstat.setString(2, imeKlase);
				boolean t = pstat.execute();
				if (!t) {
					r = t;
				}
			}

			return r;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dodajKlase(igraId, noveKlase);
			}
		}

		return false;
	}

	public static boolean dodajLika (int userId, int razina, List<String> vjestine, String naziv, int igraId, int klasaId) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("INSERT INTO lik (userID, imeLik, igra, klasa, razina, vjestine) VALUES (?, ?, ?, ?, ?, ?)");
			pstat.setInt(1, userId);
			pstat.setString(2, naziv);
			pstat.setInt(3, igraId);
			pstat.setInt(4, klasaId);
			pstat.setInt(5, razina);
			String pom = new String();
			boolean prvi = true;
			for (String s : vjestine) {
				if (prvi) {
					pom = s;
					prvi = false;
				} else {
					pom = pom + "," + s;
				}
			}
			pstat.setString(6, pom);
			boolean r = pstat.execute();


			return r;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dodajLika(userId, razina, vjestine, naziv, igraId, klasaId);
			}
		}

		return false;
	}

	public static boolean azurirajLika (int userId, int likId, int razina, List<String> vjestine) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("UPDATE lik SET userID = ? , razina = ? , vjestine = ? WHERE likID = ?");
			pstat.setInt(1, userId);
			pstat.setInt(2, razina);
			String pom = new String();
			boolean prvi = true;
			for (String s : vjestine) {
				if (prvi) {
					pom = s;
					prvi = false;
				} else {
					pom = pom + "," + s;
				}
			}
			pstat.setString(3, pom);
			pstat.setInt(4, likId);
			boolean r = pstat.execute();


			return r;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return azurirajLika(userId, likId, razina, vjestine);
			}
		}

		return false;
	}


	public static boolean dodajClanstvo (int cehId, int likId, int userId, Ovlast uloga) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("INSERT INTO clanstvo (cehID, likID, userID, uloga) VALUES (?, ?, ?, ?)");
			pstat.setInt(1, cehId);
			pstat.setInt(2, likId);
			pstat.setInt(3, userId);
			if (uloga.equals(Ovlast.VODA)) {
				pstat.setString(4, "vođa");
			} else if (uloga.equals(Ovlast.KOORDINATOR)) {
				pstat.setString(4, "koordinator");
			} else if (uloga.equals(Ovlast.CLAN)) {
				pstat.setString(4, "clan");
			}
			boolean r = pstat.execute();


			return r;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dodajClanstvo(cehId, likId, userId, uloga);
			}
		}

		return false;
	}

	public static int dohvatiCehId(String naziv) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT cehID FROM ceh WHERE imeCeh = ?");
			pstat.setString(1, naziv);
			ResultSet rsc = pstat.executeQuery();rsc.next();
			int cehId = rsc.getInt("cehID");

			return cehId;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dohvatiCehId(naziv);
			}
		}

		return -1;
	}

	public static boolean dodajPrijavu(int cehId, int likId, int userId, String tekst) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("INSERT INTO prijave (cehID, likID, userID, sadrzaj) VALUES (?, ?, ?, ?)");
			pstat.setInt(1, cehId);
			pstat.setInt(2, likId);
			pstat.setInt(3, userId);
			pstat.setString(4, tekst);
			boolean r = pstat.execute();

			return r;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				dodajPrijavu(cehId, likId, userId, tekst);
			}
		}
		return false;
	}

	public static List<Dogadaj> likDohvatiDogadaje(int likId, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT dogID FROM dogadajprijave WHERE likID = ?");
			pstat.setInt(1, likId);
			ResultSet rs = pstat.executeQuery();

			ArrayList<Dogadaj> dogadaji = new ArrayList<>();
			while (rs.next()) {
				int dogId = rs.getInt("dogID");

				pstat = con.prepareStatement("SELECT cehID, imeDogadaj, opis, vrijeme FROM dogadaj WHERE dogID = ?");
				pstat.setInt(1, dogId);
				ResultSet rsd = pstat.executeQuery(); rsd.next();

				int cehId = rsd.getInt("cehID");
				String imeDog = rsd.getString("imeDogadaj");
				String opis = rsd.getString("opis");
				Timestamp vrijeme = rsd.getTimestamp("vrijeme");
				LocalDateTime vrijemeL = vrijeme.toLocalDateTime();

				Dogadaj d = new Dogadaj(dogId, cehId, imeDog, opis, "vrijeme", svrha);
				if (!dogadaji.contains(d)) {
					dogadaji.add(d);
				}
			}

			return dogadaji;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return likDohvatiDogadaje(likId, svrha);
			}
		}

		return null;
	}

	public static Korisnik obavijestDohvatiKorisnika(int userId, Svrha svrha) {
		Connection con = getCon();

		try {
			Korisnik k= dohvatiKorisnika(userId, svrha);
			return k;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return obavijestDohvatiKorisnika(userId, svrha);
			}
		}

		return null;
	}

	public static Igra klasaDohvatiIgru(int igraId, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT imeIgre FROM igra WHERE igraID = ?");
			pstat.setInt(1, igraId);
			ResultSet rs = pstat.executeQuery(); rs.next();

			String imeIgre = rs.getString("imeIgre");

			Igra i = new Igra(igraId, imeIgre, svrha);

			return i;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return cehDohvatiIgru(igraId, svrha);
			}
		}

		return null;
	}

	public static Igra likDohvatiIgru(int igraId, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT imeIgre FROM igra WHERE igraID = ?");
			pstat.setInt(1, igraId);
			ResultSet rs = pstat.executeQuery(); rs.next();

			String imeIgre = rs.getString("imeIgre");

			Igra i = new Igra(igraId, imeIgre, svrha);

			return i;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return cehDohvatiIgru(igraId, svrha);
			}
		}

		return null;
	}

	public static boolean azurirajCeh(int cehId, String opis, String postignuca) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("UPDATE ceh SET opis = ? , postignuca = ? WHERE cehID = ?");
			pstat.setString(1, opis);
			pstat.setString(2, postignuca);
			pstat.setInt(3, cehId);
			boolean r = pstat.execute();

			return r;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return azurirajCeh(cehId, opis, postignuca);
			}
		}

		return false;
	}

	public static boolean dodajObavijest(int cehId, int likId, String tekst) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("INSERT INTO obavijest (cehID, autor, sadrzaj) VALUES (?, ?, ?)");
			pstat.setInt(1, cehId);
			pstat.setInt(2, likId);
			pstat.setString(3, tekst);
			boolean r = pstat.execute();

			return r;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				dodajObavijest(cehId, likId, tekst);
			}
		}
		return false;
	}

	public static boolean dodajDogadajPrijava(int likId, int dogadajId) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("INSERT INTO dogadajprijave (likID, dogID) VALUES (?, ?)");
			pstat.setInt(1, likId);
			pstat.setInt(2, dogadajId);
			boolean r = pstat.execute();

			return r;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				dodajDogadajPrijava(likId, dogadajId);
			}
		}
		return false;
	}

	public static boolean obrisiDogadajPrijava(int likId, int dogadajId) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("DELETE FROM dogadajprijave WHERE likID = ? AND dogID = ?");
			pstat.setInt(1, likId);
			pstat.setInt(2, dogadajId);
			boolean r = pstat.execute();

			return r;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return obrisiDogadajPrijava(likId, dogadajId);
			}
		}

		return false;
	}

	public static boolean dodajDogadaj(int cehId, String naslov, String datum) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("INSERT INTO dogadaj (cehID, imeDogadaj, ) VALUES (?, ?, ?)");
			pstat.setInt(1, cehId);
			pstat.setString(2, naslov);
			DateTimeFormatter f = DateTimeFormatter.ofPattern("dd.MM.yyyy");
			LocalDate date = LocalDate.parse(datum, f);
			Timestamp ts = Timestamp.valueOf(date.atStartOfDay());
			pstat.setTimestamp(3, ts);
			boolean r = pstat.execute();

			return r;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dodajDogadaj(cehId, naslov, datum);
			}
		}

		return false;
	}

	public static boolean clanUpgrade(int clanstvoId) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT uloga FROM clanstvo WHERE clanstvoID = ?");
			pstat.setInt(1, clanstvoId);
			ResultSet rs = pstat.executeQuery();

			if (!rs.next()) {
				return false;
			}


			String uloga = rs.getString("uloga");
			String novaUloga = new String();
			if (uloga.equalsIgnoreCase("vođa") || uloga.equalsIgnoreCase("voda")) {
				return false;
			} else if (uloga.equalsIgnoreCase("koordinator")) {
				novaUloga = "vođa";
			} else if (uloga.equalsIgnoreCase("član") || uloga.equalsIgnoreCase("clan")) {
				novaUloga = "koordinator";
			}

			pstat = con.prepareStatement("UPDATE clanstvo SET uloga = ? WHERE clanstvoID = ?");
			pstat.setString(1, novaUloga);
			pstat.setInt(2, clanstvoId);
			boolean r = pstat.execute();

			return r;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return clanUpgrade(clanstvoId);
			}
		}

		return false;
	}

	public static boolean clanDowngrade(int clanstvoId) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT uloga FROM clanstvo WHERE clanstvoID = ?");
			pstat.setInt(1, clanstvoId);
			ResultSet rs = pstat.executeQuery();

			if (!rs.next()) {
				return false;
			}

			String uloga = rs.getString("uloga");
			String novaUloga = new String();
			if (uloga.equalsIgnoreCase("vođa") || uloga.equalsIgnoreCase("voda")) {
				novaUloga = "koordinator";
			} else if (uloga.equalsIgnoreCase("koordinator")) {
				novaUloga = "član";
			} else if (uloga.equalsIgnoreCase("član") || uloga.equalsIgnoreCase("clan")) {
				return false;
			}

			pstat = con.prepareStatement("UPDATE clanstvo SET uloga = ? WHERE clanstvoID = ?");
			pstat.setString(1, novaUloga);
			pstat.setInt(2, clanstvoId);
			boolean r = pstat.execute();

			return r;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return clanDowngrade(clanstvoId);
			}
		}

		return false;
	}




	public static boolean dodajCilj(int cehId, String nazivCilj) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("INSERT INTO cilj (cehID, imeCilj) VALUES (?, ?)");
			pstat.setInt(1, cehId);
			pstat.setString(2, nazivCilj);
			boolean r = pstat.execute();
			return r;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dodajCilj(cehId, nazivCilj);
			}
		}

		return false;
	}

	public static boolean ispuniCilj(int ciljId) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("UPDATE cilj SET postignut = ? WHERE ciljID = ?");
			pstat.setBoolean(1, true);
			pstat.setInt(2, ciljId);
			boolean r = pstat.execute();

			return r;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return ispuniCilj(ciljId);
			}
		}

		return false;
	}

	public static List<Poruka> dohvatiPoruke(int userId, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT * FROM poruka WHERE posiljatelj = ? OR primatelj = ? ");
			pstat.setInt(1, userId);
			pstat.setInt(2, userId);
			ResultSet rs = pstat.executeQuery();

			ArrayList<Poruka> poruke = new ArrayList<>();
			while (rs.next()) {
				int porukaId = rs.getInt("porukaID");
				int pos = rs.getInt("posiljatelj");
				int prim = rs.getInt("primatelj");
				String sadrzaj = rs.getString("sadrzaj");
				LocalDateTime vrijeme = rs.getTimestamp("vrijeme").toLocalDateTime();

				Korisnik posiljatelj = dohvatiKorisnika(pos, svrha);
				Korisnik primatelj = dohvatiKorisnika(prim, svrha);

				Poruka p = new Poruka(0, posiljatelj.getId(), primatelj.getId(), sadrzaj, "vrijeme", false, svrha);
				if (!poruke.contains(p)) {
					poruke.add(p);
				}
			}

			return poruke;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dohvatiPoruke(userId, svrha);
			}
		}

		return new ArrayList<>();
	}

	public static boolean obrisiSudjelovanje(int likId, int ciljId) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("DELETE FROM sudjelovanje WHERE likID = ? AND ciljID = ?");
			pstat.setInt(1, likId);
			pstat.setInt(2, ciljId);
			boolean r =pstat.execute();

			return r;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return obrisiSudjelovanje(likId, ciljId);
			}
		}

		return false;
	}

	public static List<Dogadaj> likDohvatiNadolazeceDogadaje(int likId, Svrha svrha) {
		List<Dogadaj> dogadaji = likDohvatiDogadaje(likId, svrha);

		LocalDateTime trenutno = LocalDateTime.now();
		List<Dogadaj> nadolazeciDogadaji = new ArrayList<>();
		for (Dogadaj dog : dogadaji) {
			//if (trenutno.isBefore(dog.getVrijeme())) {
				nadolazeciDogadaji.add(dog);
			//}
		}
		return nadolazeciDogadaji;
	}

	public static boolean obrisiPodcilj(int podciljId) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("DELETE FROM podcilj WHERE podciljID = ?");
			pstat.setInt(1, podciljId);
			boolean r = pstat.execute();

			return r;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return obrisiPodcilj(podciljId);
			}
		}

		return false;
	}

	public static List<Integer> dohvatiSudionikeId(int ciljId) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT likID FROM sudjelovanje WHERE ciljID = ?");
			pstat.setInt(1, ciljId);
			ResultSet rs = pstat.executeQuery();

			ArrayList<Integer> sudionici = new ArrayList<>();
			while (rs.next()) {
				int sudionikId = rs.getInt("likID");
				if (!sudionici.contains(sudionikId)) {
					sudionici.add(sudionikId);
				}
			}

			return sudionici;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dohvatiSudionikeId(ciljId);
			}
		}

		return null;
	}

	public static boolean dodajSudionika(int likId, int ciljId) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("INSERT INTO sudjelovanje (likID, ciljID) VALUES (?, ?)");
			pstat.setInt(1, likId);
			pstat.setInt(2, ciljId);
			boolean r = pstat.execute();
			return r;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dodajSudionika(likId, ciljId);
			}
		}

		return false;
	}

	public static boolean dodajPodcilj(String naziv, int ciljId) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("INSERT INTO podcilj (imepodcilj, ciljID) VALUES (?, ?)");
			pstat.setString(1, naziv);
			pstat.setInt(2, ciljId);
			boolean r = pstat.execute();
			return r;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dodajPodcilj(naziv, ciljId);
			}
		}

		return false;
	}

	public static boolean ispuniPodcilj(int podciljId) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("UPDATE podcilj SET postignut = ? WHERE podciljID = ?");
			pstat.setBoolean(1, true);
			pstat.setInt(2, podciljId);
			boolean r = pstat.execute();

			return r;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return ispuniPodcilj(podciljId);
			}
		}

		return false;
	}




	public static boolean korisnikAzurirajOpis(String opis, int userId) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("UPDATE ceh SET about = ? WHERE userID = ?");
			pstat.setString(1, opis);
			pstat.setInt(2, userId);
			boolean r = pstat.execute();

			return r;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return korisnikAzurirajOpis(opis, userId);
			}
		}

		return false;
	}

	/**
	 * Novi unos u relaciju Glasanje
	 * @param cehId
	 * @param userId
	 * @param glasId
	 * @return
	 */
	public static boolean dajGlas(int cehId, int userId, int glasId) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("INSERT INTO glasanje (cehID, glasacID, glasID) VALUES (?, ?, ?)");
			pstat.setInt(1, cehId);
			pstat.setInt(2, userId);
			pstat.setInt(3, glasId);
			boolean r = pstat.execute();

			return r;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				dajGlas(cehId, userId, glasId);
			}
		}
		return false;
	}

	/**
	 * Provjerava jesu li svi koordinatori glasali
	 * ako jesu, postavlja korisnika sa najvise glasova u vođu
	 */
	public static boolean provjeriGlasove(int cehId) {
		Connection con = getCon();

		try {
			List<Korisnik> nisuGlasaliK = cehDohvatiKoordinatore(cehId, Svrha.RAZGOVORI);
			ArrayList<Integer> nisuGlasali = new ArrayList<>();
			for (Korisnik k : nisuGlasaliK) {
				PreparedStatement prestat = con.prepareStatement("SELECT clanstvoID FROM clanstvo WHERE (cehID = ?) AND (userID = ?)");
				prestat.setInt(1, cehId);
				prestat.setInt(2, k.getId());
				ResultSet rsc = prestat.executeQuery();

				if (rsc.next()) {
					int clanstvoId = rsc.getInt("clanstvoID");
					if (!nisuGlasali.contains(clanstvoId)) {
						nisuGlasali.add(clanstvoId);
					}
				}
			}

			PreparedStatement pstat = con.prepareStatement("SELECT glasacID FROM glasanje WHERE (cehID = ?) AND (glasID IS NOT NULL)");
			pstat.setInt(1, cehId);
			ResultSet rs = pstat.executeQuery();

			while (rs.next()) {
				int jeGlasao = rs.getInt("glasacID");
				if (nisuGlasali.contains(jeGlasao)) {
					nisuGlasali.remove(jeGlasao);
				}
			}
			if (!nisuGlasali.isEmpty()) {
				return false;
			}



			pstat = con.prepareStatement("SELECT glasID FROM glasanje WHERE cehID = ?");
			pstat.setInt(1, cehId);
			rs = pstat.executeQuery();

			Map<Integer, Integer> prebrojavanje = new HashMap<>();
			while (rs.next()) {
				int glasId = rs.getInt("glasID");
				if (prebrojavanje.keySet().contains(glasId)) {
					int trenutnibroj = prebrojavanje.get(glasId);
					trenutnibroj = trenutnibroj+1;
					prebrojavanje.put(glasId, trenutnibroj);
				} else {
					prebrojavanje.put(glasId, 1);

				}
			}

			int najvise = 0;
			for (Integer broj : prebrojavanje.values()) {
				if (broj>najvise) {
					najvise = broj;
				}
			}

			int pobjednikId = -1;
			for (Integer clanstvoId : prebrojavanje.keySet()) {
				if (prebrojavanje.get(clanstvoId).equals(najvise)) {
					pobjednikId = clanstvoId;
				}
			}

			return clanUpgrade(pobjednikId);

		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return provjeriGlasove(cehId);
			}
		}

		return false;
	}

	/**
	 * Vraca sve igre u kojima postoje likovi bez ceha
	 * @return
	 */
	public static List<Igra> dohvatiIgreZaLikoveBezCeha(int userId, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT likID FROM lik WHERE userID = ?");
			pstat.setInt(1, userId);
			ResultSet rs = pstat.executeQuery();
			ArrayList<Integer> likovi = new ArrayList<>();
			while(rs.next()) {
				int likId = rs.getInt("likID");
				if (!likovi.contains(likId)) {
					likovi.add(likId);
				}
			}

			pstat = con.prepareStatement("SELECT likID FROM clanstvo");
			rs = pstat.executeQuery();
			ArrayList<Integer> likoviUNekomCehu = new ArrayList<>();
			while(rs.next()) {
				int likId = rs.getInt("likID");
				if (!likoviUNekomCehu.contains(likId)) {
					likoviUNekomCehu.add(likId);
				}
			}

			ArrayList<Integer> likoviBezCeha = new ArrayList<>();
			for (Integer likId : likovi) {
				if (!likoviUNekomCehu.contains(likId)) {
					likoviBezCeha.add(likId);
				}
			}

			ArrayList<Igra> igre = new ArrayList<>();
			for (Integer likId : likoviBezCeha) {
				Igra i = likDohvatiIgru(likId, svrha);
				if (!igre.contains(i)) {
					igre.add(i);
				}
			}

			return igre;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dohvatiIgreZaLikoveBezCeha(userId, svrha);
			}
		}

		return null;
	}


	public static List<Lik> igraDohvatiLikoveBezCeha(int userId, int igraId, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT likID FROM lik WHERE userID = ? AND igraID = ?");
			pstat.setInt(1, userId);
			pstat.setInt(2, igraId);
			ResultSet rs = pstat.executeQuery();

			ArrayList<Integer> likoviBezCeha = new ArrayList<>();
			while(rs.next()) {
				int likId = rs.getInt("likID");

				pstat = con.prepareStatement("SELECT clanstvoID FROM clanstvo WHERE likID = ?");
				pstat.setInt(1, likId);
				ResultSet rsc = pstat.executeQuery();

				if (!rsc.next()) {
					if (!likoviBezCeha.contains(likId)) {
						likoviBezCeha.add(likId);
					}
				}
			}

			ArrayList<Lik> likoviL = new ArrayList<>();
			for (Integer lid : likoviBezCeha) {
				Lik l = dohvatiLika(lid, svrha);
				likoviL.add(l);
			}

			return likoviL;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return igraDohvatiLikoveBezCeha(userId, igraId, svrha);
			}
		}

		return null;
	}

	/**
	 * Vraca broj korisnikovih razgovora koji sadrze nepregledane poruke
	 * @param userId
	 * @return
	 */
	public static int dohvatiBrojRazgovoraSNepregledanimPorukama(int userId) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT porukaID, posiljatelj, primatelj, pregledano FROM poruka WHERE posiljatelj = ? OR primatelj = ? ");
			pstat.setInt(1, userId);
			pstat.setInt(2, userId);
			ResultSet rs = pstat.executeQuery();

			ArrayList<String> razgovori = new ArrayList<>();
			while (rs.next()) {
				int porukaId = rs.getInt("porukaID");
				Integer pos = rs.getInt("posiljatelj");
				Integer prim = rs.getInt("primatelj");
				boolean pregledano = rs.getBoolean("pregledano");

				String razgovor1 = pos.toString()+","+prim.toString();
				String razgovor2 = prim.toString()+","+pos.toString();
				if (!pregledano) {
					if (razgovori.contains(razgovor1) || razgovori.contains(razgovor2)) {
					} else {
						razgovori.add(razgovor1);
					}
				}
			}

			return razgovori.size();
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dohvatiBrojRazgovoraSNepregledanimPorukama(userId);
			}
		}

		return -1;
	}

	/**
	 * Vraca true ako je zadani korisnik clan zadanog ceha, false ako nije
	 * @param userId
	 * @param cehId
	 * @return
	 */
	public static boolean isKorisnikClan(int userId, int cehId) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT clanstvoID FROM clanstvo WHERE userID = ? AND cehID = ? ");
			pstat.setInt(1, userId);
			pstat.setInt(2, cehId);
			ResultSet rs = pstat.executeQuery();

			if (rs.next()) {
				return true;
			} else {
				return false;
			}

		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return isKorisnikClan(userId, cehId);
			}
		}

		return false;
	}

	public static List<Korisnik> dogadajDohvatiSudionike(int dogadajId, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT likID FROM dogadajprijave WHERE dogID = ?");
			pstat.setInt(1, dogadajId);
			ResultSet rs = pstat.executeQuery();

			ArrayList<Korisnik> sudionici = new ArrayList<>();
			while (rs.next()) {
				int likId = rs.getInt("likID");

				pstat = con.prepareStatement("SELECT userID FROM lik WHERE likID = ?");
				pstat.setInt(1, likId);
				ResultSet rsl = pstat.executeQuery();

				if (rsl.next()) {
					int korisnikId = rsl.getInt("userID");
					Korisnik k = dohvatiKorisnika(korisnikId, svrha);
					if (!sudionici.contains(k)) {
						sudionici.add(k);
					}
				}
			}

			return sudionici;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dogadajDohvatiSudionike(dogadajId, svrha);
			}
		}

		return null;
	}

	public static boolean isKorisnikPrijavljenNaDogadaj(int userId, int dogadajId) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT likID FROM dogadajprijave WHERE dogID = ?");
			pstat.setInt(1, dogadajId);
			ResultSet rs = pstat.executeQuery();

			boolean prijavljen = false;
			while (rs.next()) {
				int likId = rs.getInt("likID");

				pstat = con.prepareStatement("SELECT userID FROM lik WHERE likID = ?");
				pstat.setInt(1, likId);
				ResultSet rsl = pstat.executeQuery();

				if (rsl.next()) {
					Integer korisnikId = rsl.getInt("userID");
					if (korisnikId.equals(userId)) {
						prijavljen = true;
					}
				}
			}

			return prijavljen;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return isKorisnikPrijavljenNaDogadaj(userId, dogadajId);
			}
		}

		return false;
	}

	public static List<Clanstvo> cehDohvatiClanstva(int cehId, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT clanstvoID, likID, userID, uloga "
					+ "FROM clanstvo WHERE cehID = ?");
			pstat.setInt(1, cehId);
			ResultSet rs = pstat.executeQuery();

			ArrayList<Clanstvo> clanstva = new ArrayList<>();
			while (rs.next()) {
				int clanstvoId = rs.getInt("clanstvoID");
				int korisnikId = rs.getInt("userID");
				int likId = rs.getInt("likID");
				String uloga = rs.getString("uloga");

				if (uloga.equalsIgnoreCase("vođa") || uloga.equalsIgnoreCase("voda")) {
					Clanstvo c = new Clanstvo(clanstvoId, cehId, likId, korisnikId, Ovlast.VODA, svrha);
					if (!(clanstva.contains(c))) {
						clanstva.add(c);
					}
				} else if (uloga.equalsIgnoreCase("koordinator")) {
					Clanstvo c = new Clanstvo(clanstvoId, cehId, likId, korisnikId, Ovlast.KOORDINATOR, svrha);
					if (!(clanstva.contains(c))) {
						clanstva.add(c);
					}
				} else if (uloga.equalsIgnoreCase("član") || uloga.equalsIgnoreCase("clan")) {
					Clanstvo c = new Clanstvo(clanstvoId, cehId, likId, korisnikId, Ovlast.CLAN, svrha);
					if (!(clanstva.contains(c))) {
						clanstva.add(c);
					}
				}

			}

			return clanstva;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return cehDohvatiClanstva(cehId, svrha);
			}
		}

		return new ArrayList<>();
	}

	public static boolean obrisiPrijavu(int cehId, int likId) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("DELETE FROM prijave WHERE cehId = ? AND likID = ?");
			pstat.setInt(1, cehId);
			pstat.setInt(2, likId);
			boolean r = pstat.execute();

			return r;

		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return obrisiPrijavu(cehId, likId);
			}
			return false;
		}
	}

	public static boolean uclaniKorisnika(int cehId, int likId, int userId, Ovlast ovlast) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("INSERT INTO clanstvo (cehID, likID, userID, uloga) VALUES (?, ?, ?, ?)");
			pstat.setInt(1, cehId);
			pstat.setInt(2, likId);
			pstat.setInt(3, userId);
			if (ovlast.equals(Ovlast.VODA)) {
				pstat.setString(4, "vođa");
			} else if (ovlast.equals(Ovlast.KOORDINATOR)) {
				pstat.setString(4, "koordinator");
			} else if (ovlast.equals(Ovlast.CLAN)) {
				pstat.setString(4, "clan");
			}
			boolean r = pstat.execute();

			return r;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return uclaniKorisnika(cehId, likId, userId, ovlast);
			}
			return false;
		}

	}



	public static Clanstvo dohvatiClanstvo(int userId, int cehId, Svrha svrha) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT clanstvoID, likID, uloga "
					+ "FROM clanstvo WHERE userID = ? AND cehID = ?");
			pstat.setInt(1, userId);
			pstat.setInt(2, cehId);
			ResultSet rs = pstat.executeQuery();

			while (rs.next()) {
				int clanstvoId = rs.getInt("clanstvoID");
				int likId = rs.getInt("likID");
				String uloga = rs.getString("uloga");

				if (uloga.equalsIgnoreCase("vođa") || uloga.equalsIgnoreCase("voda")) {
					Clanstvo c = new Clanstvo(clanstvoId, cehId, likId, userId, Ovlast.VODA, svrha);
					return c;

				} else if (uloga.equalsIgnoreCase("koordinator")) {
					Clanstvo c = new Clanstvo(clanstvoId, cehId, likId, userId, Ovlast.KOORDINATOR, svrha);
					return c;

				} else if (uloga.equalsIgnoreCase("član") || uloga.equalsIgnoreCase("clan")) {
					Clanstvo c = new Clanstvo(clanstvoId, cehId, likId, userId, Ovlast.CLAN, svrha);
					return c;
				}

			}

		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dohvatiClanstvo(userId, cehId, svrha);
			}
		}

		return null;

	}

	/**
	 * Vraca postignuca iz relacije Ceh kao listu stringova
	 */
	public static List<String> dohvatiIstaknutaPostignucaZaCeh(int cehId) {
		Connection con = getCon();

		try {
			PreparedStatement pstat = con.prepareStatement("SELECT postignuca FROM ceh WHERE cehID = ?");
			pstat.setInt(1, cehId);
			ResultSet rs = pstat.executeQuery();
			String postignuca = new String();
			if (rs.next()) {
				postignuca = rs.getString("postignuca");
			}

			List<String> listaPostignuca;
			if(postignuca != null && !postignuca.equals("")){
				listaPostignuca = Arrays.asList(postignuca.trim().split(","));
			}else{
				listaPostignuca = new ArrayList<>();
			}

			return listaPostignuca;
		} catch(Exception e) { e.printStackTrace(); Messages.addMessage(e.getCause() + ": " + e.getMessage()); FacesContext.getCurrentInstance().renderResponse();
			if (reconnect()) {
				return dohvatiIstaknutaPostignucaZaCeh(cehId);
			}
		}
		return new ArrayList<>();
	}


}