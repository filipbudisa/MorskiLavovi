(function($){
	$(document).ready(function(){
		if($(".filter_igra").length){
			var id = $(".filter_igra").val();

			$(".filter_likovi option").each(function(){
				if($(this).parent("span.toggleOption").length == 0){
					$(this).hide();
					$(this).wrap("<span class='toggleOption' style='display: none;' />");
				}
			});

			$(".filter_likovi option[data-igra='" + id + "']").each(function(){
				$(this).unwrap();
				$(this).show();
			});
		}

		$(".filter_igra").change(function(){
			var id = $(this).val();

			$(".filter_likovi option").each(function(){
				if($(this).parent("span.toggleOption").length == 0){
					$(this).hide();
					$(this).wrap("<span class='toggleOption' style='display: none;' />");
				}
			});

			$(".filter_likovi option[data-igra='" + id + "']").each(function(){
				$(this).unwrap();
				$(this).show();
			});
		});

		$(".brisanje_ceha .ceh_obrisi").click(function(e){
			overlayOpen($("#overlay_brisanje"));
			e.preventDefault();
		});

		$(".nova_obavijest").click(function(e){
			overlayOpen($("#overlay_obavijest"));
			e.preventDefault();
		});

		$(".nova_poruka").click(function(e){
			overlayOpen($("#overlay_poruka"));
			e.preventDefault();
		});

		$(".novi_dogadaj").click(function(e){
			overlayOpen($("#overlay_dogadaj"));
			e.preventDefault();
		});

		$(".sudionika").click(function(e){
			var sudionici = $(this).parent().parent().siblings(".sudionici");

			if(sudionici.hasClass("shown")){
				sudionici.slideUp();
				sudionici.removeClass("shown");
			}else{
				sudionici.slideDown();
				sudionici.addClass("shown");
			}

			e.preventDefault();
		});

		$(".dropdown .header").click(function(e){
			var $dropdown = $(this).parent();

			if($dropdown.hasClass("open")){
				$dropdown.find(".body").slideUp();
				$dropdown.removeClass("open");
			}else{
				$dropdown.find(".body").slideDown();
				$dropdown.addClass("open");
			}
		});

		/* ### */

		function updateCiljevi(){
			var ciljevi = [];

			$(".cilj_uredivanje").each(function(){
				var $cilj = $(this);

				var id = $cilj.attr("data-id");

				var noviPodciljevi = [];
				var podciljevi = [];

				$cilj.find(".podciljevi").children().each(function(){
					var $podcilj = $(this);
					var id = $podcilj.attr("data-id");

					if(id === undefined){
						noviPodciljevi.push($podcilj.find("h3").text());
					}else{
						var ispunjen = $podcilj.attr("data-isp");
						var podcilj = {
							id: id,
							ispunjen: (ispunjen !== undefined && ispunjen == "da")
						};
						podciljevi.push(podcilj);
					}
				});

				var sudionici = [];

				$cilj.find(".sudionici").children().each(function(){
					var $sudionik = $(this);
					var sudionik = $sudionik.attr("data-id");
					if(!sudionici.includes(sudionik)) sudionici.push(sudionik);
				});

				var cilj = {
					sudionici: sudionici,
					noviPodciljevi: noviPodciljevi,
					podciljevi: podciljevi,
					id: id
				};

				ciljevi.push(cilj);
			});

			$(".izmjeneCiljeva").val(JSON.stringify(ciljevi));
		}

		function makniSudionik($sudionik){
			var sudionikId = $sudionik.parent().attr("data-id");
			var ciljId = $sudionik.parent().parent().parent().attr("data-id");

			if(ciljId === undefined || sudionikId === undefined) return;

			var maknutiSudioniciStr = $(".obrisanaSudjelovanja").val();
			var maknutiSudionici;
			if(maknutiSudioniciStr == "") maknutiSudionici = {};
			else maknutiSudionici = JSON.parse($(".obrisanaSudjelovanja").val());

			var cilj;

			if(maknutiSudionici[ciljId] !== undefined) cilj = maknutiSudionici[ciljId];
			else cilj = [];

			if(!cilj.includes(sudionikId)) cilj.push(sudionikId);
			maknutiSudionici[ciljId] = cilj;

			$(".obrisanaSudjelovanja").val(JSON.stringify(maknutiSudionici));
		}

		var $sudionikParent, $podciljParent;

		$("#overlay_sudionik form").submit(function(e){
			var $sudionik = $(this).find("select").find(":selected");
			var korisnik = $sudionik.attr("data-korisnik");
			var lik = $sudionik.attr("data-lik");
			var id = $sudionik.parent().val();

			var $noviSudionik = $("<p />").addClass("sudionik");

			var $sudionikInfo = $("<a />");
			var $sudionikKorisnik = $("<span />").html(korisnik);
			$sudionikInfo.html($sudionikKorisnik[0].outerHTML + " " + lik);

			var $sudionikMakni = $("<a />").addClass("sudionik_makni");
			$sudionikMakni.click(function(f){
				makniSudionik($(this));
				$(this).parent().remove();
				updateCiljevi();
				f.preventDefault();
			});

			$noviSudionik.append($sudionikInfo);
			$noviSudionik.append($sudionikMakni);

			$noviSudionik.attr("data-id", id);

			$sudionikParent.append($noviSudionik);

			overlayClose();
			updateCiljevi();

			e.preventDefault();
		});

		$(".sudionik_makni").click(function(e){
			makniSudionik($(this));
			$(this).parent().remove();
			updateCiljevi();
			e.preventDefault();
		});

		$(".podcilj_zavrsi").click(function(e){
			$(this).parent().attr("data-isp", "da");
			$(this).remove();
			updateCiljevi();
			e.preventDefault();
		});

		function makniPodcilj($podcilj){
			var podciljId = $podcilj.parent().attr("data-id");
			if(podciljId === undefined) return;

			var maknutiPodciljevi = $(".obrisaniPodciljevi").val();

			if(maknutiPodciljevi == "") maknutiPodciljevi = podciljId;
			else maknutiPodciljevi += "," + podciljId;

			$(".obrisaniPodciljevi").val(maknutiPodciljevi);
		}

		$("#overlay_podcilj a").click(function(e){
			var podciljNaziv = $("#overlay_podcilj input").val();

			var $noviPodcilj = $("<div />").addClass("podcilj_uredivanje box pad mar");

			var $podciljnaslov = $("<h3 />");
			$podciljnaslov.html(podciljNaziv);

			var $podciljMakni = $("<a />").addClass("podcilj_makni");

			$podciljMakni.click(function(f){
				makniPodcilj($(this));
				$(this).parent().remove();
				updateCiljevi();
				f.preventDefault();
			});

			$noviPodcilj.append($podciljnaslov);
			$noviPodcilj.append($podciljMakni);

			$podciljParent.append($noviPodcilj);

			updateCiljevi();
			overlayClose();
			e.preventDefault();
		});

		$(".podcilj_makni").click(function(f){
			makniPodcilj($(this));
			$(this).parent().remove();
			updateCiljevi();
			f.preventDefault();
		});

		$(".podcilj_dodaj").click(function(e){
			$podciljParent = $(this).siblings(".podciljevi");
			overlayOpen($("#overlay_podcilj"), function(){
				$("#overlay_podcilj input").val("");
				$podciljParent = null;
			});
			e.preventDefault();
		});

		$(".sudionik_dodaj").click(function(e){
			$sudionikParent = $(this).parent().parent().siblings(".sudionici");
			overlayOpen($("#overlay_sudionik"), function(){
				$sudionikParent = null;
			});
			e.preventDefault();
		});

		$(".cilj_dodaj").click(function(e){
			overlayOpen($("#overlay_cilj"));
			e.preventDefault();
		});

		/* ### */

		function updateIgre(){
			var igre = [];

			$(".igre .igra").each(function(){
				var $igra = $(this);
				var id = $igra.attr("data-id");

				var noveKlase = [];
				$igra.find("ul.klase li").each(function(){
					if($(this).attr("data-id") === undefined)
						noveKlase.push($(this).find(".left").text());
				});

				var igra = {
					noveKlase: noveKlase
				};

				if(id === undefined){
					igra.naziv = $igra.find(".header h3").text();
					igra.novaIgra = true;
				}else{
					igra.id = id;
					igra.novaIgra = false;
				}

				igre.push(igra);
			});

			$("input.izmjeneIgra").val(JSON.stringify(igre));
		}

		var $klasaParent, $igraBrisParent;

		function igraAttachControls(igra){
			igra.find(".klasa_makni").click(function(e){
				var idKlase = $(this).parent().attr("data-id");

				if(idKlase !== undefined){
					var obrisaneKlase = $("input.obrisaneKlase").val();

					if(obrisaneKlase == "") obrisaneKlase = idKlase;
					else obrisaneKlase += "," + idKlase;

					$("input.obrisaneKlase").val(obrisaneKlase);
				}

				$(this).parent().remove();

				updateIgre();

				e.preventDefault();
			});

			igra.find(".klasa_dodaj").click(function(e){
				$klasaParent = $(this).parent().parent();

				overlayOpen($("#overlay_klasa"), function(){
					$("#overlay_klasa").find("input").val("");
					$klasaParent = null;
				});

				e.preventDefault();
			});

			igra.find(".igra_obrisi").click(function(e){
				$igraBrisParent = $(this).parent().parent();

				overlayOpen($("#overlay_igrabris"), function(){
					$igraBrisParent = null;
				});

				e.preventDefault();
			});
		}

		igraAttachControls($(".igre .igra"));

		$("#overlay_klasa a").click(function(e){
			var $val = $("#overlay_klasa input").val();

			var $klTekst = $("<span />");
			$klTekst.addClass("left");
			$klTekst.html($val);

			var $klMakni = $("<a />");
			$klMakni.addClass("klasa_makni right");

			$klMakni.click(function(e){
				var idKlase = $(this).parent().attr("data-id");

				if(idKlase !== undefined){
					var obrisaneKlase = $("input.obrisaneKlase").val();

					if(obrisaneKlase == "") obrisaneKlase = idKlase;
					else obrisaneKlase += "," + idKlase;

					$("input.obrisaneKlase").val(obrisaneKlase);
				}

				$(this).parent().remove();

				updateIgre();

				e.preventDefault();
			});

			var $novaKlasa = $("<li />");
			$novaKlasa.append($klTekst);
			$novaKlasa.append($klMakni);

			$klasaParent.find("ul").append($novaKlasa);

			updateIgre();

			overlayClose();

			e.preventDefault();
		});

		$("#overlay_igrabris a").click(function(e){
			var id = $igraBrisParent.attr("data-id");
			var obrisani = $("input.obrisaneIgre").val();

			if(id !== undefined){
				if(obrisani == "") obrisani = id;
				else obrisani += "," + id;
			}

			$("input.obrisaneIgre").val(obrisani);

			$igraBrisParent.remove();
			overlayClose();
			updateIgre();
			e.preventDefault();
		});

		$("#overlay_igra a").click(function(e){
			var parent = $(this).parent();
			var naziv = parent.find("input").val();

			var $igraContainer = $("<div />").addClass("box mar igra dropdown");

			var $header = $("<div />").addClass("header");
			$header.append($("<h3 />").html(naziv));

			var $body = $("<div />").addClass("body pad");

			var $linija = $("<div />").addClass("linija");
			$linija.append($("<span />").addClass("left").html("Klase:"));
			$linija.append($("<a href='' />").addClass("right klasa_dodaj"));

			var $linijaKlase = $("<div />").addClass("linija");
			$linijaKlase.append($("<ul />").addClass("klase"));

			$body.append($linija);
			$body.append($linijaKlase);
			$body.append($("<a />").addClass("igra_obrisi").html("Obriši"));

			$igraContainer.append($header);
			$igraContainer.append($body);

			igraAttachControls($igraContainer);
			$header.click(function(e){
				var $dropdown = $(this).parent();

				if($dropdown.hasClass("open")){
					$dropdown.find(".body").slideUp();
					$dropdown.removeClass("open");
				}else{
					$dropdown.find(".body").slideDown();
					$dropdown.addClass("open");
				}
			});

			$(".igre").append($igraContainer);

			updateIgre();

			overlayClose();
			e.preventDefault();
		});

		$(".igra_dodaj").click(function(e){
			overlayOpen($("#overlay_igra"));
			e.preventDefault();
		});

		function likAttachControls(lik){
			lik.find(".vjestina_makni").click(function(e){
				$(this).parent().remove();

				updateLikovi();

				e.preventDefault();
			});

			lik.find(".vjestina_dodaj").click(function(e){
				$vjestinaParent = $(this).parent().parent();

				overlayOpen($("#overlay_vjestina"), function(){
					$("#overlay_vjestina").find("input").val("");
					$vjestinaParent = null;
				});

				e.preventDefault();
			});

			lik.find(".lik_obrisi").click(function(e){
				$likbrisParent = $(this).parent().parent();

				overlayOpen($("#overlay_likbris"), function(){
					$likbrisParent = null;
				});

				e.preventDefault();
			});

			lik.find(".razina input").change(updateLikovi);
		}

		var $vjestinaParent;

		$("#overlay_vjestina a").click(function(e){
			var $val = $("#overlay_vjestina input").val();

			var $vjTekst = $("<span />");
			$vjTekst.addClass("left");
			$vjTekst.html($val);

			var $vjMakni = $("<a />");
			$vjMakni.addClass("vjestina_makni right");

			$vjMakni.click(function(e){
				$(this).parent().remove();

				e.preventDefault();
			});

			var $novaVjestina = $("<li />");
			$novaVjestina.append($vjTekst);
			$novaVjestina.append($vjMakni);

			$vjestinaParent.find("ul").append($novaVjestina);

			updateLikovi();

			overlayClose();

			e.preventDefault();
		});


		var $likbrisParent;

		$("#overlay_likbris a").click(function(e){
			var id = $likbrisParent.attr("data-id");
			var obrisani = $("input.obrisaniLikovi").val();

			if(id !== undefined){
				if(obrisani == "") obrisani = id;
				else obrisani += "," + id;
			}

			$("input.obrisaniLikovi").val(obrisani);

			$likbrisParent.remove();
			overlayClose();
			e.preventDefault();
		});

		$(".lik_dodaj").click(function(e){
			overlayOpen($("#overlay_lik"));
			e.preventDefault();
		});

		likAttachControls($(".dropdown.lik"));

		$("#overlay_lik a").click(function(e){
			var parent = $(this).parent();
			var naziv = parent.find("#ime").val();
			var razina = parent.find("#razina").val();
			var igra = parent.find("#igra").find(":selected").text();
			var klasa = parent.find("#klasa").find(":selected").text();

			var $likContainer = $("<div />").addClass("box mar lik dropdown");

			var $header = $("<div />").addClass("header");
			$header.append($("<h3 />").html(naziv));

			var $body = $("<div />").addClass("body pad");

			var $linija1 = $("<div />").addClass("linija");
			$linija1.append($("<span />").addClass("left").html(igra));
			$linija1.append($("<span />").addClass("right").html(klasa));

			var $linija2 = $("<div />").addClass("linija razina");
			$linija2.append($("<span />").addClass("left").html("Razina"));
			$linija2.append($("<input type='text' />").addClass("right").val(razina));

			var $linija3 = $("<div />").addClass("linija");
			$linija3.append($("<span />").addClass("left").html("Vještine:"));
			$linija3.append($("<a href='' />").addClass("right vjestina_dodaj"));

			var $linija4 = $("<div />").addClass("linija");
			$linija4.append($("<ul />").addClass("vjestine"));

			$body.append($linija1);
			$body.append($linija2);
			$body.append($linija3);
			$body.append($linija4);
			$body.append($("<a />").addClass("lik_obrisi").html("Obriši"));

			$likContainer.append($header);
			$likContainer.append($body);

			likAttachControls($likContainer);
			$header.click(function(e){
				var $dropdown = $(this).parent();

				if($dropdown.hasClass("open")){
					$dropdown.find(".body").slideUp();
					$dropdown.removeClass("open");
				}else{
					$dropdown.find(".body").slideDown();
					$dropdown.addClass("open");
				}
			});

			$(".likovi .container").append($likContainer);

			updateLikovi();

			overlayClose();
			e.preventDefault();
		});

		var $postignuca = jQuery("input.postignuca");

		function updatePostignuca(){
			var postignuca = "";

			$("#postignuca_uredivanje > .postignuce_ceh").each(function(){
				var $postignuce = $(this);
				var prikazi = $postignuce.find("input.prikazi").prop("checked");
				var id = $postignuce.attr("data-id");

				if(prikazi && id !== undefined){
					if(postignuca == "") postignuca = id;
					else postignuca += "," + id;
				}
			});

			$postignuca.val(postignuca);
		}

		$("#postignuca_uredivanje").sortable({
			cursor: "grabbing",
			update: updatePostignuca
		});

		$("#postignuca_uredivanje > .postignuce_ceh input.prikazi").change(updatePostignuca);

		var $likovi = $("input.likovi");

		function updateLikovi(){
			var likovi = [];

			$(".likovi .lik").each(function(){
				var $lik = $(this);
				var id = $lik.attr("data-id");

				var razina = $lik.find(".razina input").val();
				if(isNaN(razina)) razina = 0;

				var vjestine = [];
				$lik.find("ul.vjestine li").each(function(){
					vjestine.push($(this).find(".left").text());
				});

				var lik = {
					razina: razina,
					vjestine: vjestine
				};

				if(id === undefined){
					lik.naziv = $lik.find(".header h3").text();
					lik.igra = $lik.find(".igra").attr("data-id");
					lik.klasa = $lik.find(".klasa").attr("data-id");
					lik.noviLik = true;
				}else{
					lik.id = id;
					lik.noviLik = false;
				}

				likovi.push(lik);
			});

			$likovi.val(JSON.stringify(likovi));
		}

		/* ### */

		var $overlayed, closeFunc, $overlay = $("#overlay_blur");

		function overlayOpen(overlayed, func){
			$overlayed = overlayed;
			closeFunc = func;

			$overlay.show();
			$overlayed.show();

			setTimeout(function(){
				$overlay.css("opacity", "1");
				$overlayed.css("opacity", "1");
			}, 100);
		}

		function overlayClose(){
			$overlay.css("opacity", "0");
			$overlayed.css("opacity", "0");

			setTimeout(function(){
				$overlay.hide();
				$overlayed.hide();

				if(closeFunc != null) closeFunc();

				$overlayed = null;
				closeFunc = null;
			}, 500);
		}

		$("#overlay_blur").click(overlayClose);
	});
})($);